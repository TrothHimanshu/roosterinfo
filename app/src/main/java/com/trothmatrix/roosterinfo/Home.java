package com.trothmatrix.roosterinfo;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.trothmatrix.roosterinfo.Adapters.RidersAdapter;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.DefaultAlertPopUp;
import com.trothmatrix.roosterinfo.Classes.OkButtonPopUpInterface;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.LocationUpdates.GoogleService;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationPref;
import com.trothmatrix.roosterinfo.LocationUpdates.MyService;
import com.trothmatrix.roosterinfo.LocationUpdates.ServerUpdatesScheduler;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationUpdates;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationUpdatesBroadcastReceiver;
import com.trothmatrix.roosterinfo.LoginSignUp.Login;
import com.trothmatrix.roosterinfo.Model.NearByRider;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;
import com.trothmatrix.roosterinfo.Model.TrackingData;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Timer;
import java.util.TimerTask;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class Home extends AppCompatActivity implements
        GoogleMap.OnCameraIdleListener, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener {

    Context context;

    com.trothmatrix.roosterinfo.databinding.ActivityHomeBinding binding;

    Bundle savedInstanceState;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_home);
        this.savedInstanceState = savedInstanceState;


        context = this;

        String df = Controller.CurrentUser.getUserId(context);


        binding.share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String log = LocationPref.getLog(context);

                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);


                sendIntent.putExtra(Intent.EXTRA_TEXT, log);
                sendIntent.setType("text/plain");

                if (sendIntent.resolveActivity(context.getPackageManager()) != null) {

                    context.startActivity(Intent.createChooser(sendIntent, "share"));
                }

            }
        });

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {

            final String packageName = getPackageName();
            PowerManager pm = (PowerManager) getSystemService(POWER_SERVICE);
            if (!pm.isIgnoringBatteryOptimizations(packageName)) {

                DefaultAlertPopUp.OkButtonAlertPopUp(context, "Alert",
                        "In-order to allow work in the background " +
                                "Rooster Info requires permission to turn off battery optimization for this app",
                        "Ok", true, 5, new OkButtonPopUpInterface() {
                            @Override
                            public void onClickOkButton() {
                                Intent intent = new Intent();
                                intent.setAction(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS);
                                intent.setData(Uri.parse("package:" + packageName));
                                startActivity(intent);
                            }
                        });


            }
        }

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        updates = new LocationUpdates();
        SetUpRadioGroup();
        SetUpLeftNavigation();

        getGPSAndPermissions();
    }

    private void getGPSAndPermissions() {


        if (request_permission()) {

            if (!isGpsEnabled()) {
                DefaultAlertPopUp.OkButtonAlertPopUp(context, "GPS Disabled!",
                        "Enable GPS to get current location.",
                        "Ok",
                        true, R.color.blue, new OkButtonPopUpInterface() {
                            @Override
                            public void onClickOkButton() {

                                isEnableGPSClicked = true;
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });


            }
            else
            {

                    InitializeMaps();

            }

            //getLocationUpdates();
        }
    }

    private void SetUpRadioGroup() {


        binding.online.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    onlineSelected();

                    //SetUplocation();
                    createLocationRequest();

                    HitUserOnlineStatus();
                }
            }


        });

        binding.offline.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {

                    OfflineCode();

                    //   UpdateTrackingData();


                }
            }
        });


        /*if(LocationPref.isGettingUpdates(context))
        {
            binding.online.setChecked(true);

        }
        else
        {
            binding.offline.setChecked(true);

        }*/


    }


    public void OfflineMode() {
        binding.offlineTextView.setVisibility(View.VISIBLE);
        binding.ridersLabel.setVisibility(View.GONE);
        //binding.refresh.setVisibility(View.GONE);
        binding.bottomLinearLayout.setVisibility(View.GONE);
        getWindow().clearFlags(android.view.WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
       // binding.recylerView.setVisibility(View.GONE);
        //binding.recylerView.setAdapter(null);

        if (googleMap == null) {
            return;
        }

        googleMap.clear();

    }

    public void OnlineMode() {
        binding.offlineTextView.setVisibility(View.GONE);
        binding.ridersLabel.setVisibility(View.VISIBLE);
       /// binding.recylerView.setVisibility(View.VISIBLE);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        binding.ridersLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                startActivity(new Intent(context,NearByRidersActivity.class)
                .putExtra(Constants.latitude,latLng.latitude)
                        .putExtra(Constants.longitude,latLng.longitude));


            }
        });


        binding.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                //SetAnimation();

                API.GetNearByRidersAPI(context, new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {
                        //RemoveAnimation();

                        if ((boolean) response[0]) {
                            nearByRider = new NearByRider();
                            nearByRider.setNearby_riders((Nearby_riders[]) response[1]);

                            binding.ridersLabel.setVisibility(View.GONE);
                            //binding.refresh.setVisibility(View.VISIBLE);
                            binding.bottomLinearLayout.setVisibility(View.VISIBLE);

                            setUpClusterer();
                        }

                    }

                    @Override
                    public void onError(String message) {

                        //RemoveAnimation();
                    }
                });


            }
        });

        binding.hideRidersLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                binding.bottomLinearLayout.setVisibility(View.GONE);
                binding.ridersLabel.setVisibility(View.VISIBLE);
                //binding.recylerView.setAdapter(null);

                binding.backLinearLayout.setBackgroundResource(R.drawable.shape_rectangle_transparent);

            }
        });


        //BindData();

    }





    GoogleMap googleMap;

    private void InitializeMaps() {

        try {


            binding.mapview.getMapAsync(new OnMapReadyCallback() {
                @SuppressLint("MissingPermission")
                @Override
                public void onMapReady(GoogleMap mMap) {
                    googleMap = mMap;
                    googleMap.setMyLocationEnabled(true);
                    //googleMap.getUiSettings().setCompassEnabled(false);

                    if (LocationPref.isGettingUpdates(context)) {

                        TrackingData trackingData = LocationPref.getLastTrackingLocations(context);

                        if (trackingData == null) {
                            Task<Location> task = fusedLocationClient.getLastLocation();
                            if (task != null) {
                                task.addOnSuccessListener(new OnSuccessListener<Location>() {
                                    @Override
                                    public void onSuccess(Location location) {

                                        if (location != null) {
                                            latLng = new LatLng(location.getLatitude(),
                                                    location.getLongitude());
                                            AfterMapSettleCode(latLng);
                                        }

                                    }
                                });
                            }


                            return;
                        }

                        latLng = (new LatLng(Double.parseDouble(trackingData.getLatitude())
                                , Double.parseDouble(trackingData.getLongitude())));
                        // LatLng latLng34=
                        AfterMapSettleCode(latLng);
                    } else {

                        updates.getLocationUpdates(context, new ResponseInterface() {
                            @Override
                            public void onResponse(Object... response) {

                                AfterMapSettleCode((LatLng) response[0]);


                            }


                            @Override
                            public void onError(String message) {

                            }
                        });
                    }



                }
            });


            binding.mapview.onCreate(savedInstanceState);
            binding.mapview.onResume();

        } catch (Exception ex) {

        }

    }

    private void AfterMapSettleCode(LatLng latLng13) {
        latLng = latLng13;
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        //googleMap.addMarker(new MarkerOptions().position(latLng)
           //     .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));


        if (LocationPref.isGettingUpdates(context)) {
            binding.online.setChecked(true);

            onlineSelected();
            //SetUplocation();
            createLocationRequest();
            HitUserOnlineStatus();


        } else {

            binding.offline.setChecked(true);

            OfflineCode();


        }

    }

    private void SetUpLeftNavigation() {

        binding.navigationMenuImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (binding.drawer.isDrawerOpen(GravityCompat.START)) {
                    binding.drawer.closeDrawer(GravityCompat.START);
                } else {
                    binding.drawer.openDrawer(GravityCompat.START);
                }


            }
        });

        binding.logoutTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                CloseDrawer();
                OfflineCode();


                String id = Controller.CurrentUser.getUserId(context);
                Controller.CurrentUser.LogoutUser(context);

                API.LogoutAPI(context,id, new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {
                        startActivity(new Intent(context, Login.class));
                        finish();
                    }

                    @Override
                    public void onError(String message) {

                    }
                });



            }
        });


        binding.aboutUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseDrawer();


            }
        });

        binding.contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseDrawer();


            }
        });

        binding.contactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseDrawer();

                startActivity(new Intent(context, Login.class));

            }
        });


        binding.profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                CloseDrawer();


                Nearby_riders nearbyRiders = new Nearby_riders();
                nearbyRiders.setMobileno(Controller.CurrentUser.getPhone(context));
                nearbyRiders.setUsername(Controller.CurrentUser.getName(context));
                nearbyRiders.setUserImage(Controller.CurrentUser.getImage(context));


                if(latLng==null)
                {
                    nearbyRiders.setLat("0.0");
                    nearbyRiders.setLon("0.0");
                }
                else
                {
                    nearbyRiders.setLat(String.valueOf(latLng.latitude));
                    nearbyRiders.setLon(String.valueOf(latLng.longitude));
                }

                nearbyRiders.setId(Controller.CurrentUser.getUserId(context));

                startActivity(new Intent(context, RiderDetails.class)
                        .putExtra(Constants.task, nearbyRiders)

                );


                // Nearby_riders nearbyRider = new Nearby_riders();
                // nearbyRider.setUsername("temp");
                // nearbyRider.setLat("30.0");
                // nearbyRider.setLon("70.1");
                // nearbyRider.setMobileno("34343");
                // nearbyRider.setId(Controller.CurrentUser.getUserId(context));
//
                // context.startActivity(new Intent(context, RiderDetails.class)
                //         .putExtra(Constants.task,nearbyRider));
//
                // return;

            }
        });


    }


    public void CloseDrawer() {
        if (binding.drawer.isDrawerOpen(GravityCompat.START)) {
            binding.drawer.closeDrawer(GravityCompat.START);
        }
    }


    // Map Code


    public Boolean isGpsEnabled() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }



    public Location location;
    public LatLng latLng;
    private FusedLocationProviderClient fusedLocationClient;
    LocationUpdates updates;


    private boolean request_permission() {


        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        // Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        //Manifest.permission.READ_PHONE_STATE,
                        //Manifest.permission.READ_EXTERNAL_STORAGE,
                        //Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 5);
            }
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {


                getGPSAndPermissions();

                //SetUplocation();

            } else {

                ToastMessage.showMessage(context, "Permission denied. please enable permission..");

            }
        }

        if (requestCode == 6) {
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

               updates.getLocationUpdates(context, new ResponseInterface() {
                   @Override
                   public void onResponse(Object... response) {
                       AfterMapSettleCode((LatLng) response[0]);
                   }

                   @Override
                   public void onError(String message) {

                   }
               });

            } else {

                ToastMessage.showMessage(context, "Permission denied. please enable permission..");

            }
        }

        if (requestCode == REQUEST_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length <= 0) {
                // If user interaction was interrupted, the permission request is cancelled and you
                // receive empty arrays.
                //  Log.i(TAG, "User interaction was cancelled.");
            } else if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Permission was granted. Kick off the process of building and connecting
                // GoogleApiClient.
                //buildGoogleApiClient();
            } else {
                // Permission denied.


                ToastMessage.showMessage(context, "Permission denied");
                Intent intent = new Intent();
                intent.setAction(
                        Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
                Uri uri = Uri.fromParts("package",
                        BuildConfig.APPLICATION_ID, null);
                intent.setData(uri);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
            }
        }


    }


    boolean isEnableGPSClicked = false;

    @Override
    public void onResume() {
        super.onResume();
        if(googleMap!=null)
        {
            binding.mapview.onResume();
        }


        if (isEnableGPSClicked) {
            isEnableGPSClicked = false;
            getGPSAndPermissions();


            //SetUplocation();
        } else {
            updates.onResume();
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        if(googleMap!=null)
        {
            binding.mapview.onPause();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }

    public void setTextWhite(TextView textView) {
        textView.setTextColor(context.getResources().getColor(R.color.white));

    }

    public void setTextGrey(TextView textView) {
        textView.setTextColor(context.getResources().getColor(R.color.graydark));

    }


    NearByRider nearByRider;


    public void OfflineCode() {
        offlineSelected();
        removeLocationUpdates();

        HitOfflineUserStatus();

        if (googleMap == null) {
            return;
        }

        if (latLng == null) {
            return;
        }

        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        //googleMap.addMarker(new MarkerOptions().position(latLng)
           //     .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker)));


    }

    private void UpdateTrackingData() {


        API.UpdateTrackingLocationsAPI(this, new ResponseInterface() {
            @Override
            public void onResponse(Object... response) {

            }

            @Override
            public void onError(String message) {

            }
        });


    }

    private void HitOfflineUserStatus() {

        if (latLng == null) {
            return;
        }

        API.UpdateUserStatusAPI(context, String.valueOf(latLng.latitude)
                , String.valueOf(latLng.longitude), "offline", new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {

                    }

                    @Override
                    public void onError(String message) {

                    }
                });

    }

    private void HitUserOnlineStatus() {

        if (latLng == null) {
            return;
        }

        API.UpdateUserStatusAPI(context, String.valueOf(latLng.latitude)
                , String.valueOf(latLng.longitude), "online", new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {

                        if ((boolean) response[0]) {


                            //new ServerUpdatesScheduler().scheduleTimer(context);
                            new ServerUpdatesScheduler().scheduleJob(context);

                            //nearByRider = (NearByRider) response[1];
                            //setUpClusterer();
                        }

                    }

                    @Override
                    public void onError(String message) {
                        new ServerUpdatesScheduler().scheduleJob(context);
                    }
                });
    }

    private void offlineSelected() {

        OfflineMode();


        binding.online.setBackgroundResource(R.drawable.shape_rectangle_left_round_stroke_grey);
        setTextGrey(binding.online);

        binding.offline.setBackgroundResource(R.drawable.shape_rectangle_right_solid_grey);
        setTextWhite(binding.offline);

        binding.backLinearLayout.setBackgroundResource(R.drawable.shape_rectangle_top_round_solid_grey);

    }

    private void onlineSelected() {

        OnlineMode();

        binding.ridersLabel.setVisibility(View.VISIBLE);
        //binding.refresh.setVisibility(View.GONE);
        binding.bottomLinearLayout.setVisibility(View.GONE);

        binding.online.setBackgroundResource(R.drawable.shape_rectangle_left_round_solid_red);
        setTextWhite(binding.online);

        binding.offline.setBackgroundResource(R.drawable.shape_rectangle_right_stroke_grey_home);
        setTextGrey(binding.offline);

        binding.backLinearLayout.setBackgroundResource(R.drawable.shape_rectangle_transparent);


    }


    public void setUpClusterer() {


        markers = new Hashtable<String, String>();

        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);

        try {
            googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

        } catch (Exception ex) {
            String df = "";
        }

        addItems();

       // BindData();


    }

    public Marker markerGlobal;
    public Hashtable<String, String> markers;
    ArrayList<Marker> markerArrayList;
    String clickedId;

    @Override
    public void onCameraIdle() {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //marker.showInfoWindow();
        context.startActivity(new Intent(context, RiderDetails.class)
                .putExtra(Constants.task, clickedService)
                .putExtra(Constants.latitude, latLng.latitude)
                .putExtra(Constants.longitude, latLng.longitude));

    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;

        public CustomInfoWindowAdapter() {
            view = getLayoutInflater().inflate(R.layout.custom_info_window,
                    null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            if (marker != null
                    && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            try {


                markerGlobal = marker;

                //Utilities.showMessage(context,"clicked");
                String url = null;


                Marker temp = markerArrayList.get(markerArrayList.indexOf(markerGlobal));

                clickedId = String.valueOf(temp.getTag());
                url = markers.get(clickedId);

                SearchAndSetClickedService();
            /*if (marker.getId() != null && markers != null && markers.size() > 0) {
                if ( markers.get(marker.getId()) != null &&
                        markers.get(marker.getId()) != null) {
                    url = markers.get(marker.getId());
                }
            }*/
                final ImageView image = ((ImageView) view.findViewById(R.id.badge));

                if (url != null && !url.equalsIgnoreCase("null")
                        && !url.equalsIgnoreCase("")) {

                    Glide.with(context)
                            .asBitmap()
                            .load("")
                            .placeholder(R.drawable.ic_user)
                            .centerCrop()
                            .into(new BitmapImageViewTarget(image) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                    circularBitmapDrawable.setCornerRadius(4.0f);


                                    image.setImageDrawable(circularBitmapDrawable);
                                }
                            });


                } else {
                    image.setImageResource(R.drawable.ic_user);
                }

                final String title = marker.getTitle();
                final TextView titleUi = ((TextView) view.findViewById(R.id.title));
                if (title != null) {
                    titleUi.setText(title);
                } else {
                    titleUi.setText("");
                }

            /*final String snippet = marker.getSnippet();
            final TextView snippetUi = ((TextView) view
                    .findViewById(R.id.snippet));
            if (snippet != null) {
                snippetUi.setText(snippet);
            } else {
                snippetUi.setText("");
            }*/

            } catch (Exception ex) {

            }
            return view;
        }
    }


    Nearby_riders clickedService;

    private void SearchAndSetClickedService() {
        for (int i = 0; i < nearByRider.getNearby_riders().length; i++) {
            if (nearByRider.getNearby_riders()[i].getId().equalsIgnoreCase(clickedId)) {
                clickedService = nearByRider.getNearby_riders()[i];
            }
        }
    }


    @SuppressLint("MissingPermission")
    private void addItems() {

        try {


            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            if (nearByRider != null) {

                markerArrayList = new ArrayList<>();
                //mClusterManager.clearItems();
                googleMap.clear();
                if (latLng != null) {

                    builder.include(latLng);
                }
                for (int i = 0; i < nearByRider.getNearby_riders().length; i++) {


                    markers.put(nearByRider.getNearby_riders()[i].getId(), nearByRider.getNearby_riders()[i].getUserImage());

                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(nearByRider.getNearby_riders()[i].getLat()),
                                    Double.parseDouble(nearByRider.getNearby_riders()[i].getLon())))
                            .title(nearByRider.getNearby_riders()[i].getUsername())
                            //.snippet("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));


                    Marker tr = googleMap.addMarker(markerOptions);
                    tr.setTag(String.valueOf(nearByRider.getNearby_riders()[i].getId()));
                    markerArrayList.add(tr);

                    builder.include(new LatLng(Double.parseDouble(nearByRider.getNearby_riders()[i].getLat())
                            , Double.parseDouble(nearByRider.getNearby_riders()[i].getLon())));

                }

                LatLngBounds bounds = builder.build();


                if (builder != null && bounds != null) {

                    int width = getResources().getDisplayMetrics().widthPixels;
                    width = (int) (width * 0.30);

                    int height = getResources().getDisplayMetrics().heightPixels;
                    height = (int) (height * 0.30);

                    int padding = (int) (width * 0.20); // offset from edges of the map 10% of screen

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                    //googleMap.animateCamera(cu);

                }
                //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 0));


                //googleMap.

            }


        } catch (Exception ex) {

            String df = "";
        }


    }


    // locattion updates


    public static final long UPDATE_INTERVAL = 10 * 1000;

    /**
     * The fastest rate for active location updates. Updates will never be more frequent
     * than this value, but they may be less frequent.
     */
    // FIXME: 5/14/17
    public static final long FASTEST_UPDATE_INTERVAL = UPDATE_INTERVAL / 2;

    public static final long MAX_WAIT_TIME = UPDATE_INTERVAL * 2;


    private LocationRequest mLocationRequest;

    public static Location locMap;

    @SuppressLint("MissingPermission")
    private void createLocationRequest() {

        //locationRequest = LocationRequest.create();
        //locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        //locationRequest.setInterval(20 * 1000);

        //fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, getMainLooper());

        mLocationRequest = LocationRequest.create();

        mLocationRequest.setInterval(UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(MAX_WAIT_TIME);
        // mLocationRequest.set


        try {
            //Log.i(TAG, "Starting location updates");

            /*if (LocationPref.isGettingUpdates(context)) {

                if (AppHelper.isNetworkAvailable(context)) {
                    if (LocationPref.isUpdatesViaNetworkProvider(context)) {
                        return;
                    } else {

                        String df="";
                    }
                } else {
                    fusedLocationClient.removeLocationUpdates(
                            getPendingIntent());

                    LocationPref.setGettingUpdatesViaGPSProvider(this);
                    startService(new Intent(this, MyService.class));

                    return;

                }

            }*/

            LocationPref.setBufferLocationsON(context);

            try {
                LocationPref.setGettingUpdates(context);


            } catch (Exception ex) {
                String df = "";
            }

            //new GPSJobScheduler().scheduleJob(context);
            startService(new Intent(this, GoogleService.class));
            mTimer = new Timer();
            mTimer.schedule(new TimerTaskToGetLocation(), 5, 2000);



           /* if (AppHelper.isNetworkAvailable(context))
            {
                fusedLocationClient.requestLocationUpdates(mLocationRequest, getPendingIntent());

                LocationPref.setUpdatesViaNetworkProvider(context);
            }
            else
            {
                fusedLocationClient.removeLocationUpdates(
                        getPendingIntent());

                LocationPref.setGettingUpdatesViaGPSProvider(this);
                startService(new Intent(this, MyService.class));
            }*/

            // LocationServices.FusedLocationApi.requestLocationUpdates(
            //    mGoogleApiClient, mLocationRequest, getPendingIntent());


        } catch (Exception e) {
            LocationPref.setGettingUpdatesOFF(this);
            e.printStackTrace();
        }

    }

    private Handler mHandler = new Handler();
    private Timer mTimer = null;

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    //fn_getlocation();

                    try {
                        if(locMap!=null)
                        {
                            latLng = new LatLng(locMap.getLatitude(),locMap.getLongitude());
                            googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                        }


                    }
                    catch (Exception ex)
                    {

                    }

                }
            });

        }
    }


    public void updateLoc() {




    }

    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    public PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private boolean checkPermissions() {
        int permissionState = ActivityCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION);
        return permissionState == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermissions() {
        boolean shouldProvideRationale =
                ActivityCompat.shouldShowRequestPermissionRationale(this,
                        Manifest.permission.ACCESS_FINE_LOCATION);

        // Provide an additional rationale to the user. This would happen if the user denied the
        // request previously, but didn't check the "Don't ask again" checkbox.
        if (shouldProvideRationale) {
            // Log.i(TAG, "Displaying permission rationale to provide additional context.");
            ActivityCompat.requestPermissions(Home.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            // Log.i(TAG, "Requesting permission");
            // Request permission. It's possible this can be auto answered if device policy
            // sets the permission in a given state or the user denied the permission
            // previously and checked "Never ask again".
            ActivityCompat.requestPermissions(Home.this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        }
    }

    /**
     * Callback received when a permissions request has been completed.
     */


    public void removeLocationUpdates() {
        //Log.i(TAG, "Removing location updates");
        LocationPref.setGettingUpdatesOFF(this);
        LocationPref.setBufferLocationsOFF(context);
        new ServerUpdatesScheduler().cancelJob(context);
        try {

            stopService(new Intent(context,GoogleService.class));

            mTimer.cancel();
        }
        catch (Exception ex)
        {

        }

        //new ServerUpdatesScheduler().cancelTimer(context);
        //new ServerUpdatesScheduler().cancelJob(context);

        /*if(LocationPref.isUpdatesViaNetworkProvider(context))
        {
            fusedLocationClient.removeLocationUpdates(
                    getPendingIntent());
        }
        else
        {
           try {

               stopService(new Intent(context,MyService.class));

           }
           catch (Exception ex)
           {

           }
        }*/

       // new GPSJobScheduler().cancelJob(context);

    }


}
