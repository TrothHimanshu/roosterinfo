package com.trothmatrix.roosterinfo.LoginSignUp;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.PowerManager;
import android.provider.Settings;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.DefaultAlertPopUp;
import com.trothmatrix.roosterinfo.Classes.OkButtonPopUpInterface;
import com.trothmatrix.roosterinfo.ConfirmLocation;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.R;



public class SplashScreen extends AppCompatActivity {


    Context context;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        context = this;




        if(Controller.CurrentUser.isUserLoggedIn(context))
        {
            startActivity(new Intent(context, Home.class));
            finish();
        }
        else
        {
            new Handler().postDelayed(new Runnable() {

                                          @Override
                                          public void run() {

                                              startActivity(new Intent(SplashScreen.this, Login.class));
                                              finish();

                                          }
                                      },
                    2000);
        }




    }




}
