package com.trothmatrix.roosterinfo.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.trothmatrix.roosterinfo.API;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.ConfirmLocation;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.Model.User;
import com.trothmatrix.roosterinfo.R;

public class OTPActivity extends AppCompatActivity {

    com.trothmatrix.roosterinfo.databinding.ActivityOtpBinding binding;
    Context context;

    User user = new User();
    String task = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_otp);
        context = this;
        binding = DataBindingUtil.setContentView(this, R.layout.activity_otp);

        try {

            user = (User)getIntent().getSerializableExtra(Constants.user);
            task = getIntent().getStringExtra(Constants.task);
        }
        catch (Exception ex)
        {

        }

        SetUp();

        binding.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(task.equalsIgnoreCase(Constants.loginTask))
                {
                    startActivity(new Intent(context,Login.class));
                }
                else if(task.equalsIgnoreCase(Constants.signUpTask))
                {
                    startActivity(new Intent(context,SignUp.class));
                }

                finish();

            }
        });

    }

    Boolean isP1Valid = false;
    Boolean isP2Valid = false;
    Boolean isP3Valid = false;
    Boolean isP4Valid = false;


    int counter = 1;

    public void SetUp()
    {





        binding.p1.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    binding.p2.requestFocus();

                    isP1Valid = true;

                } else {
                    isP1Valid = false;
                    binding.p1.requestFocus();
                }



            }
        });

        binding.p2.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    binding.p3.requestFocus();
                    counter = 3;
                    isP2Valid = true;

                } else {
                    isP2Valid = false;
                    binding.p1.requestFocus();
                }



            }
        });

        binding.p3.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    binding.p4.requestFocus();
                    counter = 4;
                    isP3Valid = true;

                } else {
                    isP3Valid = false;

                    binding.p2.requestFocus();
                }


            }
        });

        binding.p4.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 1) {
                    binding.submit.requestFocus();

                    isP4Valid = true;

                } else {
                    isP4Valid = false;

                    binding.p3.requestFocus();
                }



            }
        });





        binding.p4.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    binding.submit.performClick();

                }
                return false;
            }
        });


        final String[] filledCode = {""};

        binding.submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                if (binding.p1.getText().length() == 1 && binding.p2.getText().length() == 1
                        && binding.p3.getText().length() == 1 && binding.p4.getText().length() == 1) {


                    filledCode[0] = binding.p1.getText().toString().trim() +
                            binding.p2.getText().toString().trim() +  binding.p3.getText().toString().trim() +
                            binding.p4.getText().toString().trim();


                    API.VerifyOTPAPI(context, user.getMobileno(), filledCode[0], new ResponseInterface() {
                        @Override
                        public void onResponse(Object... response) {
                            if((boolean)response[0])
                            {

                                Controller.CurrentUser.setUserLoggedIn(context);

                                Controller.CurrentUser.setUserId(context,user.getId());
                                Controller.CurrentUser.setName(context,user.getName());
                                Controller.CurrentUser.setPhone(context,user.getMobileno());
                                Controller.CurrentUser.setIdRef(context,user.getIdref());
                                Controller.CurrentUser.setDeviceId(context,user.getDeviceid());



                                startActivity(new Intent(context, Home.class));
                                finish();
                            }
                        }

                        @Override
                        public void onError(String message) {

                        }
                    });




                }
                else {
                    ToastMessage.showMessage(context,"Please enter a valid OTP that we have sent on your mobile");
                }



            }
        });


    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        if(task.equalsIgnoreCase(Constants.loginTask))
        {
            startActivity(new Intent(context,Login.class));
        }
        else if(task.equalsIgnoreCase(Constants.signUpTask))
        {
            startActivity(new Intent(context,SignUp.class));
        }

        finish();
    }

}
