package com.trothmatrix.roosterinfo.LoginSignUp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import com.trothmatrix.roosterinfo.API;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.TextHelper;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Model.User;
import com.trothmatrix.roosterinfo.R;

public class SignUp extends AppCompatActivity {

    com.trothmatrix.roosterinfo.databinding.ActivitySignupBinding binding;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_signup);
        context = this;


        SetOnClickListners();
    }

    private void SetOnClickListners() {

        binding.signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(!TextHelper.ValidationHelper.ValidateListOfEditText(
                        binding.nameEditText,"Field required!",
                        binding.designationEditText,"Field required!",
                        binding.refEditText,"Field required!"
                , binding.phoneEditText,"Field required!"))
                {

                    return;
                }


                API.SignUpAPI(context,
                        binding.nameEditText.getText().toString(),
                        binding.designationEditText.getText().toString(),
                        binding.refEditText.getText().toString(),
                        binding.phoneEditText.getText().toString(),new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {
                        if((boolean)response[0])
                        {
                            startActivity(new Intent(context,OTPActivity.class)
                                    .putExtra(Constants.user,(User)response[1])
                            .putExtra(Constants.task,Constants.signUpTask));
                            finish();
                        }

                    }

                    @Override
                    public void onError(String message) {

                    }
                });



            }
        });





    }

    @Override
    public void onBackPressed() {
        //super.onBackPressed();

        startActivity(new Intent(context,Login.class));
        finish();
    }

}
