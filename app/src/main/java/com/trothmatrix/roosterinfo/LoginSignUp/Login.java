package com.trothmatrix.roosterinfo.LoginSignUp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.provider.Settings;
import android.view.View;

import com.google.android.material.behavior.HideBottomViewOnScrollBehavior;
import com.trothmatrix.roosterinfo.API;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.Model.User;
import com.trothmatrix.roosterinfo.R;

public class Login extends AppCompatActivity {


    com.trothmatrix.roosterinfo.databinding.ActivityLoginBinding binding;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_login);
        context = this;


        SetOnClickListners();
       // startActivity(new Intent(Settings.ACTION_REQUEST_IGNORE_BATTERY_OPTIMIZATIONS, Uri.parse("package:"+getPackageName())));


    }

    private void SetOnClickListners() {

        binding.loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if(binding.phoneEditText.getText().length()<10)
                {
                    ToastMessage.showMessage(context,"Invalid Mobile number!!");
                    return;
                }


                API.LoginAPI(context, binding.phoneEditText.getText().toString(), new ResponseInterface() {
                    @Override
                    public void onResponse(Object... response) {

                        if((boolean)response[0])
                        {
                            startActivity(new Intent(context,OTPActivity.class)
                            .putExtra(Constants.user,(User)response[1])
                                    .putExtra(Constants.task,Constants.loginTask));
                            finish();
                        }
                    }

                    @Override
                    public void onError(String message) {

                    }
                });



            }
        });

        binding.signUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, SignUp.class));
                finish();

            }
        });


    }
}
