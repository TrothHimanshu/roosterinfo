package com.trothmatrix.roosterinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.trothmatrix.roosterinfo.Adapters.RidersAdapter;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.DefaultAlertPopUp;
import com.trothmatrix.roosterinfo.Classes.GlideHelper;
import com.trothmatrix.roosterinfo.Classes.OkButtonPopUpInterface;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.TextHelper;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationPref;
import com.trothmatrix.roosterinfo.LoginSignUp.Login;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;
import com.trothmatrix.roosterinfo.Model.Past_route_array;
import com.trothmatrix.roosterinfo.Model.RiderDetail;
import com.trothmatrix.roosterinfo.Model.TrackingData;
import com.trothmatrix.roosterinfo.Model.User;
import com.trothmatrix.roosterinfo.directionhelpers.FetchURL;
import com.trothmatrix.roosterinfo.directionhelpers.TaskLoadedCallback;

import java.security.spec.ECField;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class RiderDetails extends AppCompatActivity implements TaskLoadedCallback {

    Context context;

    com.trothmatrix.roosterinfo.databinding.ActivityRiderDetailsBinding binding;

    Nearby_riders nearbyRiders;
    LatLng latLng = null;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rider_details);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_rider_details);


        context = this;


        try {

            nearbyRiders = (Nearby_riders)getIntent().getSerializableExtra(Constants.task);
            try {
                latLng = new LatLng(getIntent().getDoubleExtra(Constants.latitude,0.0)
                        ,getIntent().getDoubleExtra(Constants.longitude,0.0));
            }
            catch (Exception ex)
            {

            }

            binding.nameTextView.setText(nearbyRiders.getUsername());
           // binding.distanceTextView.setText(AppHelper.getDistance(latLng,nearbyRiders.getLat(),nearbyRiders.getLon()) + " km");
            binding.phoneTextView.setText(nearbyRiders.getMobileno());

            GlideHelper.LoadCircularImageMethod1(context,binding.riderImageView,R.drawable.ic_user,nearbyRiders.getUserImage());

            if(nearbyRiders.getId().equalsIgnoreCase(Controller.CurrentUser.getUserId(context)))
            {
                binding.editTextView.setVisibility(View.VISIBLE);

                    if(!TextHelper.StringIsNullOrEmpty(nearbyRiders.getLat())  &&
                            !TextHelper.StringIsNullOrEmpty(nearbyRiders.getLon()))
                    {
                        latLng = new LatLng(Double.parseDouble(nearbyRiders.getLat())
                                ,Double.parseDouble(nearbyRiders.getLon()));
                    }



            }
        }
        catch (Exception ex)
        {

        }

        binding.editTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context,UserProfileActivity.class)
                        .putExtra(Constants.task,nearbyRiders));

            }
        });

        binding.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        InitializeMaps(savedInstanceState);



    }


    public void CreateRoute()
    {
        PolylineOptions lineOptions = new PolylineOptions();

        Past_route_array[] past_route_array = riderDetail.getPast_route_array();


        if(AppHelper.isEmptyArray(riderDetail.getPast_route_array()))
        {

            if(latLng!=null)
            {
                if(latLng.latitude!=0.0 ||latLng.longitude!=0.0 )
                {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                }

            }

            return ;
        }


        LatLng origin, dest;


        origin = new LatLng(Double.parseDouble(past_route_array[0].getLat()),
                Double.parseDouble(past_route_array[0].getLon()));

        dest = new LatLng(Double.parseDouble(past_route_array[past_route_array.length-1].getLat()),
                Double.parseDouble(past_route_array[past_route_array.length-1].getLon()));

        place1 = new MarkerOptions().position(new LatLng(origin.latitude,origin.longitude)).title("Start").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
        place2 = new MarkerOptions().position(new LatLng(dest.latitude, dest.longitude)).title("End").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker_end));


        googleMap.addMarker(place1);
        googleMap.addMarker(place2);

        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();

        if(place1.getPosition()!=null)
            latLngBuilder.include(place1.getPosition());

        if(place2.getPosition()!=null)
            latLngBuilder.include(place2.getPosition());

        try {

            LatLngBounds bounds = latLngBuilder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                    AppHelper.dpToPx(context, 135));
            googleMap.animateCamera(cu);


        } catch (Exception e) {
            e.printStackTrace();
        }




        for(int i=0;i<past_route_array.length;i++)
        {

            if(nearbyRiders.getId().equalsIgnoreCase(Controller.CurrentUser.getUserId(context)))
            {

                googleMap.addMarker(new MarkerOptions()
                        .position(new LatLng(Double.parseDouble(past_route_array[i].getLat())
                        ,Double.parseDouble(past_route_array[i].getLon())))
                        .title(past_route_array[i].trackingData.getDatetime())
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin)));

            }
            else
            {
                googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(past_route_array[i].getLat())
                        ,Double.parseDouble(past_route_array[i].getLon()))).icon(BitmapDescriptorFactory.fromResource(R.drawable.icon_pin)));

            }


            // lineOptions.add(new LatLng(Double.parseDouble(past_route_array[i].getLat())
                //    ,Double.parseDouble(past_route_array[i].getLon())));


        }



        //currentPolyline = googleMap.addPolyline(lineOptions);
       String fg="";
    }


    User user;
    RiderDetail riderDetail;

    private void FetchRiderDetail() {


         API.GetRiderDetailAPI(context,
                //Controller.CurrentUser.getUserId(context)
                nearbyRiders.getId()
                , new ResponseInterface() {
            @Override
            public void onResponse(Object... response) {

                if((boolean)response[0])
                {

                    riderDetail = (RiderDetail) response[1];

                    binding.nameTextView.setText(riderDetail.getName());
                    //binding.distanceTextView.setText(user.g());
                    binding.phoneTextView.setText(riderDetail.getMobileno());

                    if(AppHelper.isEmptyArray(riderDetail.getPast_route_array()))
                    {
                        //return;
                    }


                    AfterMapCode();

                    //CreateRoute();

                   // new FetchURL(RiderDetails.this).execute(
                   //         getUrl( "driving"), "driving"
                   // "https://maps.googleapis.com/maps/api/directions/json?origin=30.359901,%2076.421198&destination=30.370845,%2076.402049&mode=driving&key=AIzaSyDEAP-BcjyZmr1ucnv_7F4eF5uXlGHbd-c&mode=driving",
                         //   "driving"



                   // );

                }

            }

            @Override
            public void onError(String message) {

            }
        });




    }

    private MarkerOptions place1,place2;

    private String getUrl(String directionMode) {


        Past_route_array[] past_route_array = riderDetail.getPast_route_array();


        if(AppHelper.isEmptyArray(riderDetail.getPast_route_array()))
        {
            return "";
        }

        LatLng origin, dest;


        origin = new LatLng(Double.parseDouble(past_route_array[0].getLat()),
                Double.parseDouble(past_route_array[0].getLon()));

        dest = new LatLng(Double.parseDouble(past_route_array[past_route_array.length-1].getLat()),
                Double.parseDouble(past_route_array[past_route_array.length-1].getLon()));

        place1 = new MarkerOptions().position(new LatLng(origin.latitude,origin.longitude)).title("Location 1").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));
        place2 = new MarkerOptions().position(new LatLng(dest.latitude, dest.longitude)).title("Location 2").icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));


        googleMap.addMarker(place1);
        googleMap.addMarker(place2);

        LatLngBounds.Builder latLngBuilder = new LatLngBounds.Builder();
        if(place1.getPosition()!=null)
            latLngBuilder.include(place1.getPosition());
        if(place2.getPosition()!=null)
            latLngBuilder.include(place2.getPosition());

        try {
            LatLngBounds bounds = latLngBuilder.build();
            CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                    AppHelper.dpToPx(context, 135));
            googleMap.animateCamera(cu);
        } catch (Exception e) {
            e.printStackTrace();
        }



        HashMap<String, String> parameters = new HashMap<String, String>();
        parameters.put("origin", origin.latitude + "%2C" + origin.longitude);
        parameters.put("destination", dest.latitude + "%2C" + dest.longitude);
        parameters.put("key", getString(R.string.google_api_key_maps));

        String waypoint= "";
        for(int i=0;i<past_route_array.length;i++)
        {


            if(i!=0)
            {
                waypoint=waypoint+"%7C";
            }
            waypoint = waypoint + "via:-"
                    + past_route_array[i].getLat()
                    +"%2C" + past_route_array[i].getLon();// +
            // "%7Cvia:-" + 34.92788 + "%2C" + 138.60008;
        }

        parameters.put("waypoints", waypoint);


        String queryStr = EncodeQueryParameters(parameters);
        String url = "https://maps.googleapis.com/maps/api/directions/json" + "?" + queryStr;

        //return url;
        return "https://maps.googleapis.com/maps/api/directions/json?origin=30.359901,%2076.421198&destination=30.370845,%2076.402049&mode=driving&key=AIzaSyDEAP-BcjyZmr1ucnv_7F4eF5uXlGHbd-c";


        // Origin of route
        /*String str_origin = "origin=" + origin.latitude + "," + origin.longitude;
        // Destination of route
        String str_dest = "destination=" + dest.latitude + "," + dest.longitude;
        // Mode
        String mode = "mode=" + directionMode;
        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + mode;

        String waypoint= "&waypoints=";//=optimize:true|
        for(int i=0;i<past_route_array.length;i++)
        {


            if(i!=0)
            {
                waypoint=waypoint+"%7C";
            }
            waypoint = waypoint + "via:-"
                    + past_route_array[i].getLat()
                    +"%2C" + past_route_array[i].getLon();// +
           // "%7Cvia:-" + 34.92788 + "%2C" + 138.60008;
        }

        parameters = parameters ;//+
                //waypoint;

        // Output format
        String output = "json";
        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters + "&key=" + getString(R.string.google_api_key_maps);
        //return url;

       return "https://maps.googleapis.com/maps/api/directions/json?origin=30.359901,%2076.421198&destination=30.370845,%2076.402049&mode=driving&key=AIzaSyDEAP-BcjyZmr1ucnv_7F4eF5uXlGHbd-c";

       */

    }

    public void getPastRouteArray()
    {

        Past_route_array[] past_route_array = riderDetail.getPast_route_array();


        if(AppHelper.isEmptyArray(riderDetail.getPast_route_array()))
        {
            return;
        }

        if(past_route_array.length<4)
        {

        }
        for(int i=2;i<past_route_array.length-2;i++)
        {

        }


    }


    public static String getHostGeneralListUrl(Context context) {
        try {


            HashMap<String, String> parameters = new HashMap<String, String>();
           // parameters.put("origin", origin.latitude + "," + origin.longitude);


            String queryStr = EncodeQueryParameters(parameters);

            if (TextHelper.StringIsNullOrEmpty(queryStr)) {
                throw new Exception();
            }

            return "https://maps.googleapis.com/maps/api/directions/json" + "?" + queryStr;

        } catch (Exception ex) {

        }
        return null;

    }


    public static String EncodeQueryParameters(HashMap<String, String> parameters) {

        try {

            if (parameters.size() == 0) {
                throw new Exception();
            }

            StringBuilder queryStr = new StringBuilder();


            for (Map.Entry<String, String> parameter : parameters.entrySet()) {
                String key = parameter.getKey();
                String val = parameter.getValue();

                queryStr.append(key).append("=").append(Uri.encode(val)).append("&");
            }

            return queryStr.toString();

        } catch (Exception ex) {
            // Ignore.
        }

        return null;
    }




    private Polyline currentPolyline;

    GoogleMap googleMap;
    private void InitializeMaps(Bundle savedInstanceState) {

        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                if(nearbyRiders.getId().equalsIgnoreCase(Controller.CurrentUser.getUserId(context)))
                {

                    googleMap.setMyLocationEnabled(true);
                        if(latLng!=null)
                        {
                            if(latLng.latitude!=0.0 ||latLng.longitude!=0.0 )
                            {
                                googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                                googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                            }

                        }
                        //return;

                    riderDetail = new RiderDetail();

                    ArrayList<TrackingData> locationList =  LocationPref.getThirtySecondTrackingLocations(context);
                           // LocationPref.LocationSaverOneDay.getLocationList(context);
                    //ToastMessage.showMessage(context,"size: " +locationList.size()+"");
                    if(locationList.isEmpty())
                    {
                        return;
                    }
                    Past_route_array[] past_route_array =new Past_route_array[locationList.size()];

                    for(int i=0;i<locationList.size();i++)
                    {
                        past_route_array[i] = new Past_route_array();

                        past_route_array[i].trackingData = locationList.get(i);

                        past_route_array[i].setLat(locationList.get(i).getLatitude()+"");
                        past_route_array[i].setLon(locationList.get(i).getLongitude()+"");

                    }
                    riderDetail.setPast_route_array(past_route_array);
                    //new FetchURL(RiderDetails.this).execute(getUrl( "driving"), "driving");
                    AfterMapCode();
                }
                else
                {
                    // InitializeMaps(savedInstanceState);
                    FetchRiderDetail();
                }




















            }
        });


        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.onResume();

    }

    public void AfterMapCode() {







        // googleMap.setMyLocationEnabled(true);


        SetUplocation();



        if(riderDetail==null)
        {
            if(latLng!=null)
            {
                if(latLng.latitude!=0.0 ||latLng.longitude!=0.0 )
                {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                }

            }
            return;
        }
        if(AppHelper.isEmptyArray(riderDetail.getPast_route_array()))
        {
            if(latLng!=null)
            {
                if(latLng.latitude!=0.0 ||latLng.longitude!=0.0 )
                {
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                }

            }
            return;
        }

        CreateRoute();


    }


    // Map Code





    public Boolean isGpsEnabled() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private void SetUplocation() {

        if (request_permission()) {

            if (!isGpsEnabled()) {
                DefaultAlertPopUp.OkButtonAlertPopUp(context, "GPS Disabled!",
                        "Enable GPS to get current location.",
                        "Ok",
                        true, R.color.blue, new OkButtonPopUpInterface() {
                            @Override
                            public void onClickOkButton() {

                                isEnableGPSClicked = true;
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });

                return;
            }

            getLocationUpdates();
        }


    }

    private LocationRequest locationRequest;
    public Location location;
    //public LatLng latLng;
    private FusedLocationProviderClient fusedLocationClient;

    @SuppressLint("MissingPermission")
    public void getLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, getMainLooper());
    }
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            fusedLocationClient.removeLocationUpdates(locationCallback);
            Log.e("got location", "loc");

            List<Location> locationList = locationResult.getLocations();
            location = locationList.get(0);


            if (location != null) {
                if (fusedLocationClient != null) {

                    //latLng = new LatLng(location.getLatitude(), location.getLongitude());


                    // googleMap.getCameraPosition().
                   // googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                   // googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                   // googleMap.addMarker(new MarkerOptions().position(latLng));


                }

            }

        }


    };



    private boolean request_permission() {


        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        // Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        //Manifest.permission.READ_PHONE_STATE,
                        //Manifest.permission.READ_EXTERNAL_STORAGE,
                        //Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 5);
            }
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5)
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                SetUplocation();

            } else {

                ToastMessage.showMessage(context, "Permission denied. please enable permission..");

            }


    }


    boolean isEnableGPSClicked = false;

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();

        if (isEnableGPSClicked) {
            isEnableGPSClicked = false;
            SetUplocation();
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }


    @Override
    public void onTaskDone(Object... values) {
        if (currentPolyline != null)
            currentPolyline.remove();
        currentPolyline = googleMap.addPolyline((PolylineOptions) values[0]);



    }
}
