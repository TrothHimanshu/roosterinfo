package com.trothmatrix.roosterinfo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;

import com.google.android.gms.maps.model.LatLng;
import com.trothmatrix.roosterinfo.Adapters.RidersAdapter;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Model.NearByRider;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class NearByRidersActivity extends AppCompatActivity {

    com.trothmatrix.roosterinfo.databinding.ActivityNearByRidersBinding binding;

    Context context = this;
    LatLng latLng;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_near_by_riders);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_near_by_riders);

        double d1 = getIntent().getDoubleExtra(Constants.latitude,30.0);
        double d2 = getIntent().getDoubleExtra(Constants.longitude,70.0);

        latLng = new LatLng(d1,d2);

        GetNearByRiders();


        binding.search.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {


               if(s.length()==0)
               {
                   BindData(nearByRider.getNearby_riders());
               }
                else
               {
                   FilterAndSortData(s.toString());
               }



            }
        });


        binding.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                AppHelper.hideKeyboard((Activity) context,binding.search);


                finish();
            }
        });


        binding.ridersLabel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                startActivity(new Intent(context, RidersOnMapsActivity.class)
                .putExtra(Constants.task,nearByRider));

            }
        });


        binding.refresh.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                GetNearByRiders();

            }
        });

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_BACK) {

            AppHelper.hideKeyboard((Activity) context,binding.search);

            finish();
            return true;
        }
        return super.onKeyDown(keyCode, event);
    }


    private void FilterAndSortData(String s) {
        if(nearByRider!=null && nearByRider.getNearby_riders().length>0) {

            List<Nearby_riders> rider = new ArrayList<>();
            for(int i=0;i<nearByRider.getNearby_riders().length;i++)
            {
                if(nearByRider.getNearby_riders()[i].getUsername().contains(s))
                {
                    rider.add(nearByRider.getNearby_riders()[i]);
                }
            }

            Nearby_riders [] rd = new Nearby_riders[rider.size()];
            rider.toArray(rd);
            BindData(rd);


        }


    }

    NearByRider nearByRider;

    private void GetNearByRiders() {
        API.GetNearByRidersAPI(context, new ResponseInterface() {
            @Override
            public void onResponse(Object... response) {
                if ((boolean) response[0]) {
                    nearByRider = new NearByRider();
                    nearByRider.setNearby_riders((Nearby_riders[]) response[1]);


                   // binding.ridersLabel.setVisibility(View.GONE);
                    // binding.refresh.setVisibility(View.VISIBLE);
                    //binding.bottomLinearLayout.setVisibility(View.VISIBLE);

                   // binding.backLinearLayout.setBackgroundResource(R.drawable.shape_rectangle_top_round_solid_grey);

                   // setUpClusterer();
                    BindData(nearByRider.getNearby_riders());
                }

            }

            @Override
            public void onError(String message) {

            }
        });

    }


    private void BindData(Nearby_riders[] nearby_riders) {

        NearByRider nearByRider1 = new NearByRider();
        nearByRider1.setNearby_riders(nearby_riders);
        binding.recylerView.setLayoutManager(new LinearLayoutManager(context, RecyclerView.VERTICAL, false));
        RidersAdapter ridersAdapter = new RidersAdapter(context,nearByRider1 , latLng);
        binding.recylerView.removeAllViews();
        binding.recylerView.removeAllViewsInLayout();

        binding.recylerView.setAdapter(ridersAdapter);
    }
}
