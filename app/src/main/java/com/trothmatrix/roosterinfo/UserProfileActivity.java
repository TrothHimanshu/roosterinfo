package com.trothmatrix.roosterinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Build;
import android.os.Bundle;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.databinding.DataBindingUtil;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.ImagePicker;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.TextHelper;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.LoginSignUp.Login;
import com.trothmatrix.roosterinfo.LoginSignUp.OTPActivity;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;
import com.trothmatrix.roosterinfo.Model.User;

import java.io.ByteArrayOutputStream;

public class UserProfileActivity extends AppCompatActivity {

    com.trothmatrix.roosterinfo.databinding.ActivityUserProfileBinding binding;
    Context context;

    Nearby_riders nearbyRiders;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_user_profile);
        context = this;

        try {
            nearbyRiders = (Nearby_riders)getIntent().getSerializableExtra(Constants.task);

            binding.phoneEditText.setText(nearbyRiders.getMobileno());
            binding.nameEditText.setText(nearbyRiders.getUsername());
            Glide.with(context)
                    .asBitmap()
                    .load(nearbyRiders.getUserImage())
                    .placeholder(R.drawable.ic_user)
                    .into(new BitmapImageViewTarget(binding.imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {

                            try {


                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            resource.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            imageParameters = new ImagePicker.ImageParameters();
                            imageParameters.byteArrayOutputStream = stream;


                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            binding.imageView.setImageDrawable(circularBitmapDrawable);

                        }catch (Exception ex)
                        {

                            String fg="";

                            Resources res = getResources();
                            Drawable drawable = res.getDrawable(R.drawable.ic_user);
                            Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                            imageParameters = new ImagePicker.ImageParameters();
                            imageParameters.byteArrayOutputStream = stream;

                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            binding.imageView.setImageDrawable(circularBitmapDrawable);

                        }

                        }
                    });


        }
        catch (Exception ex)
        {

        }

        SetOnClickListners();
    }

    private void SetOnClickListners() {

        binding.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });

        binding.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (request_permission()) {
                        GetImage();
                    }
                }

            }
        });


        binding.saveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                if (!TextHelper.ValidationHelper.ValidateListOfEditText(
                        binding.nameEditText, "Field required!",
                        binding.phoneEditText, "Field required!")) {

                    return;
                }

                if (imageParameters == null
                        || imageParameters.byteArrayOutputStream == null) {

                    Resources res = getResources();
                    Drawable drawable = res.getDrawable(R.drawable.ic_user);
                    Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
                    ByteArrayOutputStream stream = new ByteArrayOutputStream();
                    bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                    imageParameters = new ImagePicker.ImageParameters();
                    imageParameters.byteArrayOutputStream = stream;


                    //ToastMessage.showMessage(context,"Please select Image");
                    //return;
                }




                API.UpdateUserProfileAPI(context, binding.nameEditText.getText().toString(),
                        binding.phoneEditText.getText().toString()
                        , imageParameters.byteArrayOutputStream.toByteArray(), new ResponseInterface() {
                            @Override
                            public void onResponse(Object... response) {




                            }

                            @Override
                            public void onError(String message) {

                            }
                        });


            }
        });


    }





    ImagePicker.ImageParameters imageParameters = null;
    boolean isImagePickUpClicked = false;


    @SuppressLint("NewApi")
    public void  GetImage()
    {
        imageParameters = new ImagePicker.ImageParameters();
        imageParameters = ImagePicker.selectImage(UserProfileActivity.this);

        isImagePickUpClicked = true;

    }







    @RequiresApi(api = Build.VERSION_CODES.M)
    private boolean request_permission() {
        if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                ||ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {

            requestPermissions(new String[]{
                    Manifest.permission.CAMERA,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE
            }, 5);

            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5)
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                    ||ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                    ||ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)  {

                ToastMessage.showMessage(context, "Permission denied. please enable permission to continue..");

            }
            else
            {
                //imageParameters = ImagePicker.onRequestPermissionsResult(requestCode,  permissions,  grantResults,ProfileActivity.this);

                GetImage();
            }





    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //callbackManager.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);

        if(isImagePickUpClicked)
        {
            isImagePickUpClicked = false;

            imageParameters = ImagePicker.onActivityResult(requestCode, resultCode, data,UserProfileActivity.this,
                    imageParameters,binding.imageView);
        }

    }


}
