package com.trothmatrix.roosterinfo;

import android.app.Activity;
import android.content.Context;
import android.util.Log;

import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.Executor;
import com.trothmatrix.roosterinfo.Classes.JSONHelper;
import com.trothmatrix.roosterinfo.Classes.Loader;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Classes.VolleyMultipartRequest;
import com.trothmatrix.roosterinfo.Classes.VolleySingleton;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationPref;
import com.trothmatrix.roosterinfo.Model.NearByRider;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;
import com.trothmatrix.roosterinfo.Model.RiderDetail;
import com.trothmatrix.roosterinfo.Model.TrackingData;
import com.trothmatrix.roosterinfo.Model.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class API {

    public static class URLs {

        public static String BaseUrl = "http://97.74.232.79:8081";


        public static String signUpUrl = BaseUrl + "/Registration/Registration";
        public static String validateOTPUrl = BaseUrl + "/ValidateOTP";
        public static String loginUrl = BaseUrl + "/Login";

        public static String riderProfileUrl = BaseUrl + "/GetDetails/";

        public static String UpdateStatusUrl = BaseUrl + "/UpdateStatus";
        public static String UpdateUserProfileUrl = BaseUrl + "/UpdateProfile";


        public static String trackingdataUrl = BaseUrl + "/trackingdata";

        public static String NearByRidersUrl = BaseUrl + "/NearByRiders?id=";

        public static String logoutUrl = BaseUrl + "/Logout/";


    }

    public static HashMap<String, String> getPostJson(String... params) {

        HashMap<String, String> param = new HashMap<>();

        for (int i = 0; i < params.length; i = i + 2) {
            param.put(String.valueOf(params[i]), String.valueOf(params[i + 1]));
        }


        return param;

    }


    public static void LoginAPI(final Context context, String MobileNo, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");


        HashMap<String, String> hashMap = getPostJson("MobileNo", MobileNo,
                "DeviceId", AppHelper.getDeviceId(context));


        JsonObjectRequest request_json  = new JsonObjectRequest(Request.Method.POST,
                URLs.loginUrl, new JSONObject(hashMap), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(final JSONObject response) {


                    try {

                        if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                            throw new Exception("Error: Response not received from Server.");
                        }

                        //JSONObject response = new JSONObject(response);

                        Integer status = response.getInt("status");
                        Loader.getInstance().cancelCustomDialog();


                        if (status == 0) {

                            ToastMessage.showMessage(context,response.getString("message"));
                            listner.onError(response.getString("message"));
                            return;
                        }

                        if (status == 1) {

                            final User user =  new Gson().fromJson(response.getJSONObject("result_array").toString(),User.class);


                            listner.onResponse(true,user);
                        }
                    } catch (Exception ex) {

                        Loader.getInstance().cancelCustomDialog();


                    }


                }
            }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(final VolleyError error) {

                    try {
                        Loader.getInstance().cancelCustomDialog();
                        if(error.networkResponse.statusCode==404)
                        {
                            ToastMessage.showMessage(context, "Number not authorised!!");
                        }


                    }
                    catch (Exception ex)

                    {

                    }


                }
            });



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }




    public static void SignUpAPI(final Context context, String Name,
                                 String designation,
                                 String Idref, String MobileNo,
                                 final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");


        HashMap<String, String> hashMap = getPostJson(
                "Name",Name,
                "Idref", Idref,
                "MobileNo",MobileNo,
                "DeviceId", AppHelper.getDeviceId(context));



        JsonObjectRequest request_json  = new JsonObjectRequest(Request.Method.POST,
                URLs.signUpUrl, new JSONObject(hashMap), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {


                try {

                    if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    Integer status = response.getInt("status");
                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                        ToastMessage.showMessage(context,response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {

                        final User user =  new Gson().fromJson(response.getJSONObject("result_array").toString(),User.class);

                        listner.onResponse(true,user);
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();


                }




            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ((Activity) context).runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ToastMessage.showMessage(context, "Error Response !!");
                        Loader.getInstance().cancelCustomDialog();


                    }
                });

            }
        });



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }


    public static void VerifyOTPAPI(final Context context, String MobileNo,
                                    String otp, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");


        final HashMap<String, String> params = getPostJson("MobileNo", MobileNo
        ,"Otp",otp);


        JsonObjectRequest request_json  = new JsonObjectRequest(Request.Method.POST,
                URLs.validateOTPUrl,  new JSONObject(params),new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {


                try {

                    //JSONObject response = new JSONObject(response1);

                    if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    Integer status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                        ToastMessage.showMessage(context,response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {



                        listner.onResponse(true);
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {



                Loader.getInstance().cancelCustomDialog();


            }
        })
        ;



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }


    public static void GetRiderDetailAPI(final Context context, String id, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");



        StringRequest request_json  = new StringRequest(Request.Method.GET,
                URLs.riderProfileUrl+id,  new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {


                try {

                    String newJson = response1.replaceAll("\\\\", "");

                    String temp = newJson.substring(1,newJson.length()-1);
                    //JsonParser jsonParser = new JsonParser();
                    //JsonObject obj = jsonParser.parse(response1).getAsJsonObject();

                   JSONObject response = new JSONObject(temp);

                    if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    Integer status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                        ToastMessage.showMessage(context,response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {


                        RiderDetail user = new Gson().fromJson(response.getJSONObject("result_array").toString(),RiderDetail.class);

                        listner.onResponse(true,user);
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {



                Loader.getInstance().cancelCustomDialog();


            }
        })
                ;



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }


    public static void UpdateUserStatusAPI(final Context context, String latitude,
                                    String longitude,String status, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");


        final HashMap<String, String> params = getPostJson("id", Controller.CurrentUser.getUserId(context)
                ,"Lat",latitude,
                "Lon",longitude,
                "status",status);


        JsonObjectRequest request_json  = new JsonObjectRequest(Request.Method.POST,
                URLs.UpdateStatusUrl, new JSONObject(params), new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {


                try {

                    //JSONObject response = new JSONObject(response1);

                    if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    Integer status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                        ToastMessage.showMessage(context,response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {


                        NearByRider nearByRider = new Gson().fromJson(response.getJSONObject("result_array").toString(),NearByRider.class);
                        LocationPref.setBufferTime(context,response.getJSONObject("result_array").getString("frequency"));
                        listner.onResponse(true,nearByRider);
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {



                Loader.getInstance().cancelCustomDialog();
                listner.onError("message");

            }
        })/*{
            @Override
            protected Map<String, String> getParams() {

                return params;
            }
        }*/
                ;



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }

    public static void UpdateUserProfileAPI(final Context context,  final String username,

                                            final String MobileNo,
                                            final byte[] byteArray,
                                            final ResponseInterface listner) {

        Loader.getInstance().showCustomDialog(context, "");

        final HashMap<String, String> params = getPostJson(

                "id",Controller.CurrentUser.getUserId(context),
                "name",username,

                "mobileno",MobileNo);

   //     file: (Image in multipart Format)

        VolleyMultipartRequest multipartRequest = new VolleyMultipartRequest(Request.Method.POST,
                URLs.UpdateUserProfileUrl, new Response.Listener<NetworkResponse>() {
            @Override
            public void onResponse(NetworkResponse response1) {


                try {

                    String strResponse = new String(response1.data);

                    if (!JSONHelper.ValidateStringResponse(strResponse)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    JSONObject response = new JSONObject(strResponse);

                    Integer status = response.getInt("status");
                    Loader.getInstance().cancelCustomDialog();

                    if (status == 0) {

                        ToastMessage.showMessage(context, response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {

                        JSONObject jsonObject = response.getJSONObject("result_array");



                        Controller.CurrentUser.setPhone(context, MobileNo);

                        Controller.CurrentUser.setName(context, username);


                        Controller.CurrentUser.setImage(context, jsonObject.getString("image_url"));


                        ToastMessage.showMessage(context, "User data saved.");

                        listner.onResponse(response);
                    }


                } catch (Exception ex) {
                    Loader.getInstance().cancelCustomDialog();

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

                ToastMessage.showMessage(context, "Error Response !!");
                Loader.getInstance().cancelCustomDialog();

            }
        }) {
            @Override
            protected Map<String, String> getParams() {

                return params;
            }

            @Override
            protected Map<String, DataPart> getByteData() {
                Map<String, DataPart> params = new HashMap<>();
                // file name could found file base or direct access from real path
                // for now just get bitmap data from ImageView
                params.put("file", new DataPart("profile_" + UUID.randomUUID().toString() + ".jpg", byteArray, "image/jpeg"));
                //params.put("cover", new DataPart("file_cover.jpg", AppHelper.getFileDataFromDrawable(getBaseContext(), mCoverImage.getDrawable()), "image/jpeg"));

                return params;
            }
        };

        multipartRequest.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(multipartRequest);

    }



    public static void UpdateTrackingLocationsAPI(final Context context,
                                            final ResponseInterface listner) {
        JSONArray jsonArray = LocationPref.getTrackingLocationsJSONArray(context);



       // ToastMessage.showMessage(context,"array: " + jsonArray.toString());
        Log.w("array",jsonArray.toString());
        if(jsonArray.length()==0)
        {
            listner.onError("");
            return;
        }
        JSONObject js = new JSONObject();
        JSONArray jsonArray1 = new JSONArray();


        // 30 second data

        TrackingData trackingData1 = null;
        try {
            trackingData1 = new Gson().fromJson(jsonArray.get(jsonArray.length()-1).toString(), TrackingData.class);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        LocationPref.saveThirtySecondLocation(context,trackingData1);






        if(!AppHelper.isNetworkAvailable(context))
        {

            TrackingData trackingData = null;
            try {
                trackingData = new Gson().fromJson(jsonArray.get(jsonArray.length()-1).toString(), TrackingData.class);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            LocationPref.saveNoNetworkLocation(context,trackingData);
            listner.onError("message");
            return;
            //jsonArray1.put(jsonArray.get(jsonArray.length()-1));

        }
        else
        {

            try {

                JSONArray jsonArrayNoNetwork= LocationPref.getNoNetworkTrackingLocationsJSONArray(context);
                if(jsonArrayNoNetwork.length()>0)
                {
                    jsonArray1 = jsonArrayNoNetwork;
                   // jsonArray1.put(jsonArray.get(jsonArray.length()-1));

                }

                    jsonArray1.put(jsonArray.get(jsonArray.length()-1));


                js.put("trackingdata",jsonArray1);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        //Loader.getInstance().showCustomDialog(context, "");


        JsonObjectRequest request_json  = null/*{
            @Override
            protected Map<String, String> getParams() {

                return params;
            }
        }*/
                ;
        request_json = new JsonObjectRequest(Request.Method.POST,
                URLs.trackingdataUrl,js, new Response.Listener<JSONObject>() {
            @Override
            public void onResponse(final JSONObject response) {


                try {

                    //JSONObject response = new JSONObject(String.valueOf(response1.getJSONObject(0)));

                    if (!JSONHelper.ValidateJSONObjectResponse(response)) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);
                    Log.w("response",response.toString());
                    Integer status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                       // ToastMessage.showMessage(context,response.getString("message"));
                        //LocationPref.setBufferLocationsOFF(context);
                        listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {

                        Log.w("updated","Locations submitted");

                        ToastMessage.showMessage(context,"Locations submitted");
                       // LocationPref.setBufferLocationsOFF(context);

                        LocationPref.setLocationJson(context,"");
                        LocationPref.setNoNetworkLocationJson(context,"");

                        //NearByRider nearByRider = new Gson().fromJson(response.getJSONObject("result_array").toString(),NearByRider.class);

                        listner.onResponse(true,"");
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();
                    listner.onError("message");


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {


                listner.onError("message");

                Loader.getInstance().cancelCustomDialog();


            }
        });


        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }




    public static void GetNearByRidersAPI(final Context context, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");



        StringRequest request_json  = new StringRequest(Request.Method.GET,
                URLs.NearByRidersUrl+Controller.CurrentUser.getUserId(context),  new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {


                try {

                    String newJson = response1.replaceAll("\\\\", "");

                    String temp = newJson.substring(1,newJson.length()-1);
                    //JsonParser jsonParser = new JsonParser();
                    //JsonObject obj = jsonParser.parse(response1).getAsJsonObject();

                    JSONArray response = new JSONArray(temp);

                    if (response==null ) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    //Integer status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    /*if (status == 0) {

                        ToastMessage.showMessage(context,response.getString("message"));
                        listner.onError(response.getString("message"));
                        return;
                    }*/

                   // if (status == 1) {


                    Nearby_riders[] user = new Gson().fromJson(response.toString(),Nearby_riders[].class);

                        listner.onResponse(true,user);
                    //}
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {



                Loader.getInstance().cancelCustomDialog();


            }
        })
                ;



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }


    public static void LogoutAPI(final Context context, String id, final ResponseInterface listner) {


        Loader.getInstance().showCustomDialog(context, "");

        String df = Controller.CurrentUser.getUserId(context);

        String url = URLs.logoutUrl +  Controller.CurrentUser.getUserId(context);

        StringRequest request_json  = new StringRequest(Request.Method.GET,
                URLs.logoutUrl+id,  new Response.Listener<String>() {
            @Override
            public void onResponse(String response1) {


                try {

                    String newJson = response1.replaceAll("\\\\", "");

                    String temp = newJson.substring(1,newJson.length()-1);
                    //JsonParser jsonParser = new JsonParser();
                    //JsonObject obj = jsonParser.parse(response1).getAsJsonObject();

                    JSONObject response = new JSONObject(temp);

                    if (response==null ) {
                        throw new Exception("Error: Response not received from Server.");
                    }

                    //JSONObject response = new JSONObject(response);

                    int status = response.getInt("status");

                    Loader.getInstance().cancelCustomDialog();


                    if (status == 0) {

                       // ToastMessage.showMessage(context,response.getString("message"));
                        //listner.onError(response.getString("message"));
                        return;
                    }

                    if (status == 1) {


                    //Nearby_riders[] user = new Gson().fromJson(response.toString(),Nearby_riders[].class);

                    listner.onResponse(true,"");
                    }
                } catch (Exception ex) {

                    Loader.getInstance().cancelCustomDialog();


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(final VolleyError error) {



                Loader.getInstance().cancelCustomDialog();


            }
        })
                ;



        request_json.setRetryPolicy(VolleySingleton.getRetryPolicy());

        VolleySingleton.getInstance(context).addToRequestQueue(request_json);
    }




}
