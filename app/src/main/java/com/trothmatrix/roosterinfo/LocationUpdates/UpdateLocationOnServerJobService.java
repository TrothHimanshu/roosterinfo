package com.trothmatrix.roosterinfo.LocationUpdates;

import android.annotation.TargetApi;
import android.app.PendingIntent;
import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.trothmatrix.roosterinfo.API;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Home;

@TargetApi(Build.VERSION_CODES.LOLLIPOP)
public class UpdateLocationOnServerJobService extends JobService {


    @Override
    public boolean onStartJob(final JobParameters jobParameters) {

        Log.w("job scheduling time: ", LocationPref.getBufferTime(this));

        //Intent service = new Intent(context, SimpleWakefulService.class);
        //startWakefulService(context, service);

        try {


            API.UpdateTrackingLocationsAPI(this, new ResponseInterface() {
                @Override
                public void onResponse(Object... response) {

                    jobFinished(jobParameters, false);
                    //new ServerUpdatesScheduler().scheduleJob(UpdateLocationOnServerJobService.this);
                    scheduleUpdates();

                }

                @Override
                public void onError(String message) {
                    jobFinished(jobParameters, false);

                    scheduleUpdates();


                }
            });

        } catch (Exception ex) {
            jobFinished(jobParameters, false);
            scheduleUpdates();

            // new ServerUpdatesScheduler().scheduleJob(UpdateLocationOnServerJobService.this);
        }

        return false;
    }

    FusedLocationProviderClient fusedLocationProviderClient;

    public void scheduleUpdates() {
        new ServerUpdatesScheduler().scheduleJob(UpdateLocationOnServerJobService.this);

        fusedLocationProviderClient = LocationServices.getFusedLocationProviderClient(this);


        /*if (AppHelper.isNetworkAvailable(UpdateLocationOnServerJobService.this)) {
            if (!LocationPref.isUpdatesViaNetworkProvider(this)) {
                requestLocationNetworkProvider();
                ToastMessage.showMessage(context,"provider switched to google client");

                try {

                    stopService(new Intent(this,MyService.class));

                }
                catch (Exception ex)
                {

                }
            }

            return;

            //fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, getPendingIntent());
        }


            fusedLocationProviderClient.removeLocationUpdates(
                    getPendingIntent());

            LocationPref.setGettingUpdatesViaGPSProvider(this);*/


        //new GPSJobScheduler().scheduleJob(context);
        startService(new Intent(this,GoogleService.class));

        //new GPSJobScheduler().scheduleJob(context);

       // ToastMessage.showMessage(context,"provider switched to GPS");


    }


    Context context = this;
    private LocationRequest mLocationRequest;

    public void requestLocationNetworkProvider() {
        mLocationRequest = LocationRequest.create();

        mLocationRequest.setInterval(Home.UPDATE_INTERVAL);

        // Sets the fastest rate for active location updates. This interval is exact, and your
        // application will never receive updates faster than this value.
        mLocationRequest.setFastestInterval(Home.FASTEST_UPDATE_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

        // Sets the maximum time when batched location updates are delivered. Updates may be
        // delivered sooner than this interval.
        mLocationRequest.setMaxWaitTime(Home.MAX_WAIT_TIME);
        // mLocationRequest.set


        try {
            //Log.i(TAG, "Starting location updates");


            LocationPref.setBufferLocationsON(context);

            try {
                LocationPref.setGettingUpdates(context);


            } catch (Exception ex) {
                String df = "";
            }


            fusedLocationProviderClient.requestLocationUpdates(mLocationRequest, getPendingIntent());

            LocationPref.setUpdatesViaNetworkProvider(context);
           // ToastMessage.showMessage(context,"provider switched to google client");

        } catch (Exception ex) {

        }
    }


    public PendingIntent getPendingIntent() {
        Intent intent = new Intent(this, LocationUpdatesBroadcastReceiver.class);
        intent.setAction(LocationUpdatesBroadcastReceiver.ACTION_PROCESS_UPDATES);
        return PendingIntent.getBroadcast(this, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    @Override
    public boolean onStopJob(JobParameters jobParameters) {
        return false;
    }


}