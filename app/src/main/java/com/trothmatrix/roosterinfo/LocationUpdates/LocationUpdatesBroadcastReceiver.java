/*
 * Copyright (C) 2017 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.trothmatrix.roosterinfo.LocationUpdates;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.os.BatteryManager;
import android.util.Log;

import com.google.android.gms.location.LocationResult;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.DateHelper;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Model.TrackingData;

import java.util.List;

import static android.content.Context.BATTERY_SERVICE;

/**
 * Receiver for handling location updates.
 *
 * For apps targeting API level O
 * {@link android.app.PendingIntent#getBroadcast(Context, int, Intent, int)} should be used when
 * requesting location updates. Due to limits on background services,
 * {@link android.app.PendingIntent#getService(Context, int, Intent, int)} should not be used.
 *
 *  Note: Apps running on "O" devices (regardless of targetSdkVersion) may receive updates
 *  less frequently than the interval specified in the
 *  {@link com.google.android.gms.location.LocationRequest} when the app is no longer in the
 *  foreground.
 */
public class LocationUpdatesBroadcastReceiver extends BroadcastReceiver {
    private static final String TAG = "LUBroadcastReceiver";

    public static final String ACTION_PROCESS_UPDATES =
            "com.google.android.gms.location.sample.backgroundlocationupdates.action" +
                    ".PROCESS_UPDATES";

    @Override
    public void onReceive(Context context, Intent intent) {
        //BatteryManager bm = (BatteryManager)context.getSystemService(BATTERY_SERVICE);

        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_PROCESS_UPDATES.equals(action)) {
                LocationResult result = LocationResult.extractResult(intent);
                if (result != null) {
                    List<Location> locations = result.getLocations();
                  //  ToastMessage.showMessage(context,"geting updates");
                    for(int i=0;i<locations.size();i++)
                    {
                        SaveLocation(locations.get(i), context);
                        //LocationPref.saveLocation(context,locations.get(i).getLatitude(),locations.get(i).getLongitude());
                    }

                    //LocationResultHelper locationResultHelper = new LocationResultHelper(
                      //      context, locations);
                    // Save the location data to SharedPreferences.
                    //locationResultHelper.saveResults();
                    // Show notification with the location data.
                   // locationResultHelper.showNotification();
                    //Log.i(TAG, LocationResultHelper.getSavedLocationResult(context));
                }
            }
        }
    }



    public static void SaveLocation(Location location, Context context)
    {
        TrackingData trackingData = new TrackingData();
        trackingData.setLatitude(String.format("%.6f", location.getLatitude())
                //String.valueOf(location.getLatitude())
        );
        trackingData.setLongitude(String.format("%.6f", location.getLongitude())
                //String.valueOf(location.getLongitude())
        );


        trackingData.setBearing(location.getBearing()+""
                //AppHelper.getBearing(location.getLatitude(),location.getLongitude())
        );
        trackingData.setDeviceid(AppHelper.getDeviceId(context));
        trackingData.setDatetime(DateHelper.Now(LocationPref.dateFormat));
        trackingData.setSpeed("2");



        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            //int batLevel = bm.getIntProperty(BatteryManager.BATTERY_PROPERTY_CAPACITY);
            trackingData.setVin(String.valueOf(AppHelper.getBatteryPercentage(context)));

        }

        LocationPref.saveLocation(context,trackingData);

       // LocationPref.saveLocation(context,trackingData);
       // LocationPref.LocationSaverOneDay.saveLocation(context,location.getLatitude(),
               // location.getLongitude());

    }
}
