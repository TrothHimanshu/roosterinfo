package com.trothmatrix.roosterinfo.LocationUpdates;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.trothmatrix.roosterinfo.LocationUpdates.old.YourWakefulReceiver;

import java.util.Calendar;

public class ServerUpdatesScheduler {


    public void scheduleTimer(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, YourWakefulReceiver.class);
        boolean flag = (PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_NO_CREATE) == null);
        /*Register alarm if not registered already*/
        if (flag) {

            Log.w("prev alarm", "no");

            PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0,
                    intent, PendingIntent.FLAG_UPDATE_CURRENT);

            // Create Calendar obj called calendar
            Calendar calendar = Calendar.getInstance();

            /* Setting alarm for every one hour from the current time.*/
            int intervalTimeMillis = 1000 * Integer.valueOf(LocationPref.getBufferTime(context)); // 1 hour
            alarmMgr.setInexactRepeating(AlarmManager.RTC_WAKEUP,
                    calendar.getTimeInMillis(), intervalTimeMillis,
                    alarmIntent);

            Log.w("alarm", "set");

        } else {
            Log.w("prev alarm", "yes");

        }
    }

    public void cancelTimer(Context context) {
        AlarmManager alarmMgr = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);

        Intent intent = new Intent(context, YourWakefulReceiver.class);
        boolean flag = (PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_NO_CREATE) == null);
        /*Register alarm if not registered already*/

        if (flag) {
            return;
        }


        assert alarmMgr != null;
        alarmMgr.cancel(PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT));


        /*PendingIntent alarmIntent = PendingIntent.getBroadcast(context, 0,
                intent, PendingIntent.FLAG_UPDATE_CURRENT);

        // Create Calendar obj called calendar
        Calendar calendar = Calendar.getInstance();

        /* Setting alarm for every one hour from the current time.*/
       /* int intervalTimeMillis = 1000 * Integer.valueOf(LocationPref.getBufferTime(context)); // 1 hour
        alarmMgr.setRepeating(AlarmManager.RTC_WAKEUP,
                calendar.getTimeInMillis(), intervalTimeMillis,
                alarmIntent);*/
    }

    public static int JOBID = 0;

    public static boolean isJobServiceOn(Context context) {
        JobScheduler scheduler = null;
        boolean hasBeenScheduled = false;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            for (JobInfo jobInfo : scheduler.getAllPendingJobs()) {
                if (jobInfo.getId() == JOBID) {
                    hasBeenScheduled = true;
                    break;
                }
            }
        }


        return hasBeenScheduled;
    }


    public void scheduleJob(Context context) {

        try {


            if (isJobServiceOn(context)) {
                Log.w("job status: ", "already running");

                //return;
            }

            JobScheduler jobScheduler = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                jobScheduler = context.getSystemService(JobScheduler.class);

            }


            ComponentName serviceComponent = new ComponentName(context, UpdateLocationOnServerJobService.class);

            JobInfo.Builder builder = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                builder = new JobInfo.Builder(JOBID, serviceComponent);

                int v = Integer.valueOf(LocationPref.getBufferTime(context));
                //builder.setPeriodic(Integer.valueOf(LocationPref.getBufferTime(context)) * 1000);
                builder.setMinimumLatency(Integer.valueOf(LocationPref.getBufferTime(context))*1000); // wait at least
                builder.setOverrideDeadline(1000 * (Integer.valueOf(LocationPref.getBufferTime(context)) +1)); // maximum delay
                builder.setPersisted(true);
                //builder.setRequiredNetworkType(JobInfo.NETWORK_TYPE_UNMETERED); // require unmetered network
                //builder.setRequiresDeviceIdle(true); // device should be idle
                //builder.setRequiresCharging(false); // we don't care if the device is charging or not

                jobScheduler.schedule(builder.build());

                Log.w("job status: ", "started");

            }

        } catch (Exception ex) {

            String df="";
            Log.w("schduler exception: ", ex.getMessage());
        }

    }


    public void cancelJob(Context context) {
        JobScheduler scheduler = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            scheduler = (JobScheduler) context.getSystemService(Context.JOB_SCHEDULER_SERVICE);
            for (JobInfo jobInfo : scheduler.getAllPendingJobs()) {
                if (jobInfo.getId() == JOBID) {
                    scheduler.cancel(JOBID);
                    Log.w("job status: ", "Canceled");
                    break;
                }
            }
        }

    }

}
