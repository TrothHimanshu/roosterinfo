package com.trothmatrix.roosterinfo.LocationUpdates.old;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import androidx.legacy.content.WakefulBroadcastReceiver;

import com.trothmatrix.roosterinfo.API;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.LocationUpdates.LocationPref;

import java.util.logging.Handler;

import static androidx.legacy.content.WakefulBroadcastReceiver.startWakefulService;

public class YourWakefulReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        ToastMessage.showMessage(context,"ringing time: " + LocationPref.getBufferTime(context));
        Log.w("alarm","ringing time: " + LocationPref.getBufferTime(context));

            //Intent service = new Intent(context, SimpleWakefulService.class);
            //startWakefulService(context, service);

        try {


            API.UpdateTrackingLocationsAPI(context, new ResponseInterface() {
                @Override
                public void onResponse(Object... response) {

                }

                @Override
                public void onError(String message) {

                }
            });

        } catch (Exception ex) {

        }
        }
    }
