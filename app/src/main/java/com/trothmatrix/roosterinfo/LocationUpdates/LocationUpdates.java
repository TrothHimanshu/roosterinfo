package com.trothmatrix.roosterinfo.LocationUpdates;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trothmatrix.roosterinfo.Classes.DefaultAlertPopUp;
import com.trothmatrix.roosterinfo.Classes.OkButtonPopUpInterface;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.R;
import com.trothmatrix.roosterinfo.databinding.ActivityHomeBinding;

import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;
import static android.os.Looper.getMainLooper;

public class LocationUpdates implements  ActivityCompat.OnRequestPermissionsResultCallback{


    Context context;


    FusedLocationProviderClient fusedLocationClient;
    ResponseInterface listner;

    public void getLocationUpdates(Context context, ResponseInterface listner)
    {
        this.context = context;
        this.listner = listner;

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);



        SetUplocation(context);

    }




    public Boolean isGpsEnabled(Context context) {

        LocationManager manager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private void SetUplocation(final Context context) {

        if (request_permission(context)) {

            if (!isGpsEnabled(context)) {
                DefaultAlertPopUp.OkButtonAlertPopUp(context, "GPS Disabled!",
                        "Enable GPS to get current location.",
                        "Ok",
                        true, R.color.blue, new OkButtonPopUpInterface() {
                            @Override
                            public void onClickOkButton() {

                                isEnableGPSClicked = true;
                                context.startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });

                return;
            }

            getLocationUpdates();
        }


    }

    static LocationRequest locationRequest;
    Location location;
    LatLng latLng;

    @SuppressLint("MissingPermission")
    public void getLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, getMainLooper());
    }

    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            fusedLocationClient.removeLocationUpdates(locationCallback);
            Log.e("got location", "loc");

            List<Location> locationList = locationResult.getLocations();
            location = locationList.get(0);


            if (location != null) {
                if (fusedLocationClient != null) {

                    latLng = new LatLng(location.getLatitude(), location.getLongitude());

                    listner.onResponse(latLng);

                    // googleMap.getCameraPosition().


                }

            }

        }


    };



    private boolean request_permission(Context context) {


        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
               ActivityCompat.requestPermissions((Activity)context, new String[]{
                        // Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        //Manifest.permission.READ_PHONE_STATE,
                        //Manifest.permission.READ_EXTERNAL_STORAGE,
                        //Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 6);
            }
            return false;
        } else {
            return true;
        }


    }




    public  static boolean isEnableGPSClicked = false;

    public void onResume() {
      //  super.onResume();

        if (isEnableGPSClicked) {
            isEnableGPSClicked = false;
            getLocationUpdates();
        }


    }






    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {

        if (requestCode == 5)
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                SetUplocation(context);

            } else {

                ToastMessage.showMessage(context, "Permission denied. please enable permission..");

            }
    }
}
