package com.trothmatrix.roosterinfo.LocationUpdates;

import android.content.Context;
import android.content.SharedPreferences;
import android.widget.TextView;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.Gson;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.DateHelper;
import com.trothmatrix.roosterinfo.Classes.TextHelper;
import com.trothmatrix.roosterinfo.Classes.TinyDB;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Model.TrackingData;

import org.json.JSONArray;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Iterator;

public class LocationPref {

    private static String LocationKey = "LocationKey";

    public static void setLog(Context context, String role) {
        try {


            /*String log = getLog(context);
            if(TextHelper.StringIsNullOrEmpty(log))
            {
                log="";
            }
            log = log + "\n" + DateHelper.Now(" HH:MM a") + ": " + role;

            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(LocationKey, log);
            editor.apply();*/


        } catch (Exception ex) {

        }

    }

    public static String getLog(Context context) {
        try {
            return context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(LocationKey, "");


        } catch (Exception ex) {
            // Ignore.
        }

        return "";
    }




    private static String LocationLocalKey = "LocationLocalKey";


    private static String LocationSharedPref = "LocationData";
    //private static String KEY_LOCATION_UPDATES_REQUESTED = "locationupdates";
    //private static String isReceivingOnPref = "isReceivingOnPref";
    private static String locationupdatesKey = "locationupdatesKey";
    private static String BufferTimeKey = "BufferTimeKey";
    private static String locationupdatesViaKey = "locationupdatesViaKey";


    public static Boolean isGettingUpdates(Context context) {
        Boolean logged = false;
        SharedPreferences sharedPref = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE);
        if (sharedPref.contains(locationupdatesKey)) {
            if (sharedPref.getString(locationupdatesKey, "").equals("true")) {
                logged = true;
            } else {
                logged = false;
            }
        }
        return logged;
    }

    public static void setGettingUpdates(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesKey, "true");
            editor.apply();

        } catch (Exception ex) {

        }

    }

    public static void setGettingUpdatesOFF(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesKey, "false");
            editor.apply();

        } catch (Exception ex) {

        }

    }











    public static Boolean isUpdatesViaNetworkProvider(Context context) {
        Boolean logged = false;
        SharedPreferences sharedPref = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE);
        if (sharedPref.contains(locationupdatesViaKey)) {
            if (sharedPref.getString(locationupdatesViaKey, "true").equals("true")) {
                logged = true;
            } else {
                logged = false;
            }
        }
        return logged;
    }

    public static void setUpdatesViaNetworkProvider(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesViaKey, "true");
            editor.apply();

        } catch (Exception ex) {

        }

    }

    public static void setGettingUpdatesViaGPSProvider(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesViaKey, "false");
            editor.apply();

        } catch (Exception ex) {

        }

    }


    //2019-09-27 16:59:00

    public static String dateFormat = "yyyy-MM-dd HH:mm:ss";
    public static SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);

    //private static String sharedPref = "OfflineSharedPref";
    public static String jsonKey = "jsonKey";


    public static Boolean isLocationBufferingON(Context context) {
        Boolean logged = false;
        SharedPreferences sharedPref = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE);
        if (sharedPref.contains(locationupdatesKey)) {
            if (sharedPref.getString(locationupdatesKey, "").equals("true")) {
                logged = true;
            } else {
                logged = false;
            }
        }
        return logged;
    }

    public static void setBufferLocationsON(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesKey, "true");
            editor.apply();

        } catch (Exception ex) {

        }

    }

    public static void setBufferLocationsOFF(Context context) {
        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(locationupdatesKey, "false");
            editor.apply();

        } catch (Exception ex) {

        }

    }


    public static void setBufferTime(Context context, String role) {
        try {

            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(BufferTimeKey, role);
            editor.apply();


        } catch (Exception ex) {

        }

    }

    public static String getBufferTime(Context context) {
        try {
            return context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(BufferTimeKey, "30");


        } catch (Exception ex) {
            // Ignore.
        }

        return "10";
    }


    public static void setLocationJson(Context context, String CategoryJson) {


        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(jsonKey, CategoryJson);
            editor.apply();

        } catch (Exception ex) {

        }


    }


    public static ArrayList<TrackingData> getTrackingLocations(Context context) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(jsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return list;
            }

            TrackingData[] Categorys = new Gson().fromJson(json, TrackingData[].class);

            list = new ArrayList<>(Arrays.asList(Categorys));

        } catch (Exception ex) {
            // Ignore.
        }

        return list;
    }


    public static TrackingData getLastTrackingLocations(Context context) {


        TrackingData trackingData = null;
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(jsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return trackingData;
            }

            TrackingData[] Categorys = new Gson().fromJson(json, TrackingData[].class);

            if (AppHelper.isEmptyArray(Categorys)) {
                return trackingData;
            }

            trackingData = new TrackingData();

            trackingData.setLatitude(Categorys[Categorys.length - 1].getLatitude());
            trackingData.setLongitude(Categorys[Categorys.length - 1].getLongitude());

        } catch (Exception ex) {
            // Ignore.
        }

        return trackingData;
    }

    public static JSONArray getTrackingLocationsJSONArray(Context context) {


        JSONArray jsonArray = new JSONArray();
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(jsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return jsonArray;
            }

            jsonArray = new JSONArray(json);


        } catch (Exception ex) {
            // Ignore.
        }

        return jsonArray;
    }

    public static void saveLocation(Context context, TrackingData excercise) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {
            if (isLocationBufferingON(context)) {
                list = getTrackingLocations(context);
            }

            if (list.size() == 0) {
                list = new ArrayList<>();
                excercise.setDistance("0");
            } else {


                if(list.get(list.size() - 1).getLatitude().equalsIgnoreCase(excercise.getLatitude())
                        && list.get(list.size() - 1).getLongitude().equalsIgnoreCase(excercise.getLongitude()))
                {
                    return;
                }


                String dis = AppHelper.getDistance2(list.get(list.size() - 1).getLatitude()
                        , list.get(list.size() - 1).getLongitude()
                        , excercise.getLatitude(), excercise.getLongitude());

                //ArrayList<LatLng> locationList =  LocationPref.LocationSaverOneDay.getLocationList(context);

                //String dis = AppHelper.getDistance2(String.valueOf(locationList.get(0).latitude)
                 //       , String.valueOf(locationList.get(0).longitude)
                  //      , excercise.getLatitude(), excercise.getLongitude());

                int dist = Integer.parseInt(list.get(list.size() - 1).getDistance()) + Integer.parseInt(dis);

                excercise.setDistance(String.valueOf(dist));
            }

            list.add(excercise);


            TrackingData[] ex = new TrackingData[list.size()];
            list.toArray(ex);

            setLocationJson(context, new Gson().toJson(ex, TrackingData[].class));


        } catch (Exception ex) {
            // Ignore.
        }

    }




    // No Network condition

    public static String noNetworkJsonKey = "noNetworkJsonKey";


    public static void setNoNetworkLocationJson(Context context, String CategoryJson) {


        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(noNetworkJsonKey, CategoryJson);
            editor.apply();

        } catch (Exception ex) {

        }


    }



    public static JSONArray getNoNetworkTrackingLocationsJSONArray(Context context) {


        JSONArray jsonArray = new JSONArray();
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(noNetworkJsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return jsonArray;
            }

            jsonArray = new JSONArray(json);


        } catch (Exception ex) {
            // Ignore.
        }

        return jsonArray;
    }

    public static void saveNoNetworkLocation(Context context, TrackingData excercise) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {
            if (isLocationBufferingON(context)) {
                list = getNoNetworkTrackingLocations(context);
            }

            if (list.size() == 0) {
                list = new ArrayList<>();
                excercise.setDistance("0");
            } else {


                if(list.get(list.size() - 1).getLatitude().equalsIgnoreCase(excercise.getLatitude())
                        && list.get(list.size() - 1).getLongitude().equalsIgnoreCase(excercise.getLongitude()))
                {
                    return;
                }


                String dis = AppHelper.getDistance2(list.get(list.size() - 1).getLatitude()
                        , list.get(list.size() - 1).getLongitude()
                        , excercise.getLatitude(), excercise.getLongitude());

                int dist = Integer.parseInt(list.get(list.size() - 1).getDistance()) + Integer.parseInt(dis);

                excercise.setDistance(String.valueOf(dist));
            }

            list.add(excercise);


            TrackingData[] ex = new TrackingData[list.size()];
            list.toArray(ex);

            setNoNetworkLocationJson(context, new Gson().toJson(ex, TrackingData[].class));


        } catch (Exception ex) {
            // Ignore.
        }

    }



    public static ArrayList<TrackingData> getNoNetworkTrackingLocations(Context context) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(noNetworkJsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return list;
            }

            TrackingData[] Categorys = new Gson().fromJson(json, TrackingData[].class);

            list = new ArrayList<>(Arrays.asList(Categorys));

        } catch (Exception ex) {
            // Ignore.
        }

        return list;
    }








    //  30second array




    public static String ThirtySecondJsonKey = "ThirtySecondJsonKey";


    public static void setThirtySecondLocationJson(Context context, String CategoryJson) {


        try {
            SharedPreferences.Editor editor =
                    context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
            editor.putString(ThirtySecondJsonKey, CategoryJson);
            editor.apply();

        } catch (Exception ex) {

        }


    }



    public static JSONArray getThirtySecondTrackingLocationsJSONArray(Context context) {


        JSONArray jsonArray = new JSONArray();
        try {

            String json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                    .getString(ThirtySecondJsonKey, "");

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return jsonArray;
            }

            jsonArray = new JSONArray(json);


        } catch (Exception ex) {
            // Ignore.
        }

        return jsonArray;
    }

    public static void saveThirtySecondLocation(Context context, TrackingData excercise) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {

            if (LocationSaverOneDay.isToday(context)) {
                list = getThirtySecondTrackingLocations(context);
            } else {
                LocationSaverOneDay.setDate(context, System.currentTimeMillis() + "");
                list = new ArrayList<>();
            }


           /* if (isLocationBufferingON(context)) {
                list = getThirtySecondTrackingLocations(context);
            }*/


            if (list.size() == 0) {
                list = new ArrayList<>();
                excercise.setDistance("0");
            } else {


                if(list.get(list.size() - 1).getLatitude().equalsIgnoreCase(excercise.getLatitude())
                        && list.get(list.size() - 1).getLongitude().equalsIgnoreCase(excercise.getLongitude()))
                {
                    return;
                }


                String dis = AppHelper.getDistance2(list.get(list.size() - 1).getLatitude()
                        , list.get(list.size() - 1).getLongitude()
                        , excercise.getLatitude(), excercise.getLongitude());

                int dist = Integer.parseInt(list.get(list.size() - 1).getDistance()) + Integer.parseInt(dis);

                excercise.setDistance(String.valueOf(dist));
            }

            list.add(excercise);


            TrackingData[] ex = new TrackingData[list.size()];
            list.toArray(ex);

            setThirtySecondLocationJson(context, new Gson().toJson(ex, TrackingData[].class));


        } catch (Exception ex) {
            // Ignore.
        }

    }



    public static ArrayList<TrackingData> getThirtySecondTrackingLocations(Context context) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {

            String json = "";

            if (LocationSaverOneDay.isToday(context)) {

                json = context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                        .getString(ThirtySecondJsonKey, "");

            } else {
                //setDate(context,System.currentTimeMillis()+"");
                return list;
            }

            if (TextHelper.StringIsNullOrEmpty(json)) {
                return list;
            }

            TrackingData[] Categorys = new Gson().fromJson(json, TrackingData[].class);

            list = new ArrayList<>(Arrays.asList(Categorys));

        } catch (Exception ex) {
            // Ignore.
        }

        return list;
    }



    public static void saveTrackingLocations(Context context, ArrayList<TrackingData> excercise) {


        ArrayList<TrackingData> list = new ArrayList<>();
        try {
            if (isLocationBufferingON(context)) {
                list = getTrackingLocations(context);
            }


            if (list.size() == 0) {
                list = new ArrayList<>();
            }

                /*if (list.size() > 0) {
                    for (TrackingData ex : excercise) {

                        for(Iterator<TrackingData> it = list.iterator();
                            it.hasNext();) {

                            TrackingData s = it.next();


                            /*if (s.date.equalsIgnoreCase(ex.date)) {
                                it.remove();

                            }*/
                        /*}





                    }

                }*/

            list.addAll(excercise);

            TrackingData[] ex = new TrackingData[list.size()];
            for (int i = 0; i < list.size(); i++) {
                ex[i] = list.get(i);
            }
            setLocationJson(context, new Gson().toJson(ex, TrackingData[].class));


        } catch (Exception ex) {
            // Ignore.
            String gh = "";
        }

    }






     /*public static ArrayList<TrackingData> getExcercisesByDate(Context context, String date) {


            ArrayList<TrackingData> list = new ArrayList<>();
            try {

                String json = context.getSharedPreferences(sharedPref, Context.MODE_PRIVATE)
                        .getString(jsonKey, "");

                if (TextHelper.StringIsNullOrEmpty(json)) {
                    return list;
                }

                TrackingData[] Categorys = new Gson().fromJson(json, TrackingData[].class);

                //list = new ArrayList<>(Arrays.asList(Categorys));

                if (Categorys.length > 0) {
                    for (TrackingData recordlistData : Categorys) {
                        if (recordlistData.date.equalsIgnoreCase(date)) {

                            list.add(recordlistData);
                        }
                    }
                }


            } catch (Exception ex) {
                // Ignore.
            }

            return list;
        }*/


    public static class LocationSaverOneDay {

        public static void saveLocation(Context context, double latitude, double longitude) {
            try {

                TinyDB tinyDB = new TinyDB(context);
                ArrayList<String> list = new ArrayList<>();
                if (isToday(context)) {
                    list = tinyDB.getListString(LocationLocalKey);
                } else {
                    setDate(context, System.currentTimeMillis() + "");
                    list = new ArrayList<>();
                }


                if (list.isEmpty()) {
                    list = new ArrayList<>();
                }

                if(list.size()>0)
                {
                    String temp = list.get(list.size() - 1);
                    String[] array = temp.split(":");

                    if(array[0].equalsIgnoreCase(String.valueOf(latitude))
                            && array[1].equalsIgnoreCase(String.valueOf(longitude)))
                    {
                        return;
                    }
                }

                list.add(latitude + ":" + longitude);
                tinyDB.putListString(LocationLocalKey, list);
                LocationPref.setLog(context,"location saved");



            } catch (Exception ex) {

                LocationPref.setLog(context,"ecepption while saving");
                ToastMessage.showMessage(context,"ecepption while saving");
            }

        }

        public static ArrayList<LatLng> getLocationList(Context context) {

            ArrayList<LatLng> locationList = new ArrayList<>();

            try {

                TinyDB tinyDB = new TinyDB(context);
                ArrayList<String> list = new ArrayList<>();
                if (isToday(context)) {
                    list = tinyDB.getListString(LocationLocalKey);
                } else {
                    //setDate(context,System.currentTimeMillis()+"");
                    return locationList;
                }


                for (int i = 0; i < list.size(); i++) {

                    String temp = list.get(i);

                    String[] array = temp.split(":");
                    LatLng latLng = new LatLng(Double.valueOf(array[0]), Double.valueOf(array[1]));

                    locationList.add(latLng);

                }


            } catch (Exception ex) {

                locationList = new ArrayList<>();
            }

            return locationList;
        }


        private static String DateKey = "DateKey";


        public static boolean isToday(Context context) {
            boolean isToday = false;

            String date = getDate(context);
            if (TextHelper.StringIsNullOrEmpty(date)) {
                return false;
            }


            if (isSameDay(date, String.valueOf(System.currentTimeMillis()))) {
                isToday = true;
            }

            return isToday;

        }

        public static boolean isSameDay(String time1, String time2) {
            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();


            cal1.setTimeInMillis(Long.parseLong(time1));
            cal2.setTimeInMillis(Long.parseLong(time2));

            boolean sameDay = cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR) &&
                    cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR);
            return sameDay;
        }


        public static void setDate(Context context, String role) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(DateKey, role);
                editor.apply();


            } catch (Exception ex) {

            }

        }

        public static String getDate(Context context) {
            try {
                return context.getSharedPreferences(LocationSharedPref, Context.MODE_PRIVATE)
                        .getString(DateKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }

    }


}
