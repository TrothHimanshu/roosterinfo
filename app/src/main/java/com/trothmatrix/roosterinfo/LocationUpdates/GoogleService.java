package com.trothmatrix.roosterinfo.LocationUpdates;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.util.Log;

import androidx.annotation.Nullable;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.maps.model.LatLng;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.R;

import java.util.Timer;
import java.util.TimerTask;


/**
 * Created by gurpreet on 24/11/16.
 */

public class GoogleService extends Service implements LocationListener {

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    double latitude, longitude;
    LocationManager locationManager;
    Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long notify_interval = 1000;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;


    public GoogleService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    Context context = this;
    @Override
    public void onCreate() {
        super.onCreate();


        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 5, notify_interval);
        intent = new Intent(str_receiver);
        setLocationTracker();

        LocationPref.setLog(context,"Service started");
    }

    @Override
    public void onLocationChanged(Location location) {


    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        //LocationPref.setLog(context,"provider status1: " + provider);

    }

    @Override
    public void onProviderEnabled(String provider) {
        //LocationPref.setLog(context,"provider status2: " + provider);

    }

    @Override
    public void onProviderDisabled(String provider) {
       // LocationPref.setLog(context,"provider status3: " + provider);

    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        LocationPref.setLog(context,"service destroy called");

        try {
            if(LocationPref.isGettingUpdates(GoogleService.this))
            {
                startService(new Intent(this, GoogleService.class));
                LocationPref.setLog(context,"service restart");

            }
            else
            {
                mTimer.cancel();
                ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(NOTIFICATION);

                //stopSelf();
            }
        }
        catch (Exception ex)
        {

        }


    }

    private void fn_getlocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
       // LocationPref.setLog(context,"GPS_PROVIDER status: " + isGPSEnable);
       // LocationPref.setLog(context,"Network provider status: " + isNetworkEnable);

        if (!isGPSEnable && !isNetworkEnable) {

        } else {
            if (isGPSEnable) {
                location = null;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                LocationPref.setLog(context,"requested updates");

                if (locationManager != null) {
                    Location  location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    if (location != null) {
                        /*Log.e("latitude1", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");*/
                     //   all.setText(all.getText().toString() + "\n" + location.getLatitude() + " GPSP " + location.getLongitude());
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        LocationPref.setLog(context,"got GPS location");
                        fn_update(location);
                        //ToastMessage.showMessage(this,"GPS update");
                       return;
                    }
                    else
                    {
                        LocationPref.setLog(context,"GPS location null");
                    }
                }
            }
            if (isNetworkEnable) {
                /*location = null;
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this);
                if (locationManager != null) {
                    /*Criteria criteria = new Criteria();
                    String provider = locationManager.getBestProvider(criteria, false);
                     location = locationManager.getLastKnownLocation(provider);
                    */

                /*location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    if (location != null) {

                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");

                        latitude = location.getLatitude();
                        longitude = location.getLongitude();
                        fn_update(location);
                        ToastMessage.showMessage(this,"Network update");
                        LocationPref.setLog(context,"got Network location");

                    }
                    else
                    {
                        LocationPref.setLog(context,"Network location null");
                    }
                }*/

            }




        }

    }

    public void setLocationTracker() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
            String channelName = "My Background Service";
            NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName, NotificationManager.IMPORTANCE_NONE);
            chan.setLightColor(Color.BLUE);
            chan.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);
            NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            assert manager != null;
            manager.createNotificationChannel(chan);

            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID);
            Notification notification = notificationBuilder.setOngoing(true)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentTitle("Tracking")
                    .setPriority(NotificationManager.IMPORTANCE_MIN)
                    .setCategory(Notification.CATEGORY_SERVICE)
                    .build();
            startForeground(NOTIFICATION, notification);
        } else {
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0,
                    new Intent(this, Home.class)
                            .setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP),
                    0);

            NotificationCompat.BigTextStyle appCompatStyle = new NotificationCompat.BigTextStyle();
            appCompatStyle.bigText(getString(R.string.app_name));

            Notification notification = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.icon_rooster_logo)
                    .setCategory(NotificationCompat.CATEGORY_SERVICE)
                    .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
                    .setContentTitle(getString(R.string.app_name))
                    .setStyle(appCompatStyle)
                    .setTicker("Location")
                    .setContentText("Location")
                    .setWhen(System.currentTimeMillis())
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .build();

            notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.notify(NOTIFICATION, notification);
        }

        LocationPref.setLog(context,"Notification started");

    }

    private NotificationManager notificationManager;
    private static final int NOTIFICATION = 3;

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    //fn_getlocation();

                    try {

                        if(LocationPref.isGettingUpdates(GoogleService.this))
                        {
                           // LocationPref.setLog(context,"timer scheduled");

                            fn_getlocation();
                        }
                        else
                        {
                            LocationPref.setLog(context,"timer canceled");

                            mTimer.cancel();
                            ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).cancel(NOTIFICATION);
                            locationManager.removeUpdates(GoogleService.this);

                            stopSelf();
                        }

                    }
                    catch (Exception ex)
                    {

                    }

                }
            });

        }
    }

    private void fn_update(Location location) {

        saveLocation(location);

    }


    public void saveLocation(Location location)
    {
        if (location != null) {
            //ToastMessage.showMessage(this,"GPS updates");
            LocationUpdatesBroadcastReceiver.SaveLocation(location, this);


            try {

                Home.locMap = location;
                //ToastMessage.showMessage(this,"GPS updates");
            }
            catch (Exception ex)
            {
                //ToastMessage.showMessage(this,"exception");
            }

        }
    }

}