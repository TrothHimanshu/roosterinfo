package com.trothmatrix.roosterinfo.LocationUpdates;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.BatteryManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.trothmatrix.roosterinfo.Classes.NotificationHelper;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.R;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;


public class MyService extends Service implements LocationListener {

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    public static double latitude, longitude;
    LocationManager locationManager;
    public static String shareprefrence_name = "Login_Details";
    public static String TAG = "DATATAGSERVICE";
    public static SharedPreferences sharedpreferences;
    public static SharedPreferences.Editor editor;
    Location location;
    int i = 0;
    private Handler mHandler = new Handler();
    public static Timer mTimer = null;
    long notify_interval = 1000;
    //public static String str_receiver = "servicetutorial.service.receiver";
    //Intent intent;

    public MyService() {

    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }


    /*@Override
    public int onStartCommand(Intent intent, int flags, int startId) {
       // Toast.makeText(this, "service starting", Toast.LENGTH_SHORT).show();
        return super.onStartCommand(intent,flags,startId);
    }*/

    @Override
    public void onCreate() {
        super.onCreate();

        sharedpreferences = getSharedPreferences(shareprefrence_name, Context.MODE_PRIVATE);
        editor = sharedpreferences.edit();
        mTimer = new Timer();
        mTimer.schedule(new TimerTaskToGetLocation(), 2000, notify_interval);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
            startMyOwnForeground();
        else {
            /*For Normal phones less then oreo*/
            Intent notificationIntent = new Intent(MyService.this, Home.class);
            PendingIntent pendingIntent = PendingIntent.getActivity(MyService.this, 0,
                    notificationIntent, 0);

            Notification notification = new Notification.Builder(MyService.this)
                    .setContentTitle(getString(R.string.app_name))
                    .setContentText("Tracking")
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setContentIntent(pendingIntent)
                    .setTicker("Location")
                    .build();

            startForeground(10, notification);
        }
        //intent = new Intent(str_receiver);

       fn_getlocation();
    }

    @Override
    public void onLocationChanged(Location location) {

    }
    public void saveLocation(Location location)
    {
        if (location != null) {
            //ToastMessage.showMessage(this,"GPS updates");
            LocationUpdatesBroadcastReceiver.SaveLocation(location, this);
            try {

                //ToastMessage.showMessage(this,"GPS updates");
            }
            catch (Exception ex)
            {
                ToastMessage.showMessage(this,"exception");
            }

        }
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }


    private void fn_getlocation() {
        locationManager = (LocationManager) getApplicationContext().getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        /*If Both are close then its stop*/
        if (!isGPSEnable && !isNetworkEnable) {

        } else {
            if (isGPSEnable) {
                location = null;
                /*For Oreo notification*/

                /*Location with GPS provider*/
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                        return;
                    }
                }
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 1, this);
                if (locationManager != null) {
                    location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    //ToastMessage.showMessage(this,"got liocation");

                    if (location != null) {
                        Log.e("latitude", location.getLatitude() + "");
                        Log.e("longitude", location.getLongitude() + "");
                        latitude = location.getLatitude();
                        longitude = location.getLongitude();

                        saveLocation(location);
                        /*Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(new Runnable() {

                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(), location.getLatitude() + "", Toast.LENGTH_SHORT).show();
                            }
                        });*/


                    }
                }
            } else if (isNetworkEnable) {


            }


        }

    }

    Context context = this;

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startMyOwnForeground() {


        NotificationHelper notificationHelper= new NotificationHelper(context);
        notificationHelper.createTrackingNotification("Rooster Info", "Tracking","");

        /*int notifyID = 1;
        String CHANNEL_ID = "my_channel_01";// The id of the channel.
        CharSequence name = "channel_name";// The user-visible name of the channel.
        int importance = NotificationManager.IMPORTANCE_HIGH;
        NotificationChannel mChannel = new NotificationChannel(CHANNEL_ID, name, importance);
// Create a notification and set the notification channel.
        Notification notification = new Notification.Builder(MyService.this)
                .setContentTitle("New Message")
                .setContentText("You've received new messages.")
                .setSmallIcon(R.drawable.icon_rooster_logo)
                .setChannelId(CHANNEL_ID)
                .build();

        NotificationManager mNotificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            mNotificationManager.createNotificationChannel(mChannel);*/
        //}


        /*String NOTIFICATION_CHANNEL_ID = "com.example.simpleapp";
        String channelName = "My Background Service";
        NotificationChannel chan = new NotificationChannel(NOTIFICATION_CHANNEL_ID, channelName,
                NotificationManager.IMPORTANCE_HIGH);
        chan.setLightColor(Color.BLUE);
        chan.setLockscreenVisibility(Notification.VISIBILITY_PUBLIC);
        NotificationManager manager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        assert manager != null;
        manager.createNotificationChannel(chan);

        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this,
                NOTIFICATION_CHANNEL_ID);
        Notification notification = notificationBuilder.setOngoing(true)
                .setContentTitle("App is running in background")
                .setPriority(NotificationManager.IMPORTANCE_MAX)
                .setCategory(Notification.CATEGORY_SERVICE)
                .build();
        startForeground(2, notification);*/

       // ToastMessage.showMessage(this,"notification started");

    }

    /*Timer*/
    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {

            mHandler.post(new Runnable() {
                @Override
                public void run() {
                    try {

                        if(LocationPref.isGettingUpdates(MyService.this))
                        {
                            fn_getlocation();
                        }
                        else
                        {
                            stopSelf();
                        }

                    }
                    catch (Exception ex)
                    {

                    }


                }
            });

        }
    }



    @Override
    public void onDestroy() {
        //Toast.makeText(this, "service destroyed", Toast.LENGTH_SHORT).show();
    }






}