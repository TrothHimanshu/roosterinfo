package com.trothmatrix.roosterinfo;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trothmatrix.roosterinfo.Adapters.RidersAdapter;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.Controller;
import com.trothmatrix.roosterinfo.Classes.ResponseInterface;
import com.trothmatrix.roosterinfo.Model.NearByRider;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;

import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;

public class RidersOnMapsActivity extends AppCompatActivity implements
        GoogleMap.OnCameraIdleListener, GoogleMap.OnMarkerClickListener,
        GoogleMap.OnInfoWindowClickListener {

    com.trothmatrix.roosterinfo.databinding.ActivityRidersMapBinding binding;

    Context context = this;
    LatLng latLng;

    GoogleMap googleMap;
    NearByRider nearByRider;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_riders_map);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_riders_map);

        double d1 = getIntent().getDoubleExtra(Constants.latitude,30.0);
        double d2 = getIntent().getDoubleExtra(Constants.longitude,70.0);

        latLng = new LatLng(d1,d2);


        nearByRider = (NearByRider) getIntent().getSerializableExtra(Constants.task);




        binding.backImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });


        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;

                 googleMap.setMyLocationEnabled(true);

                setUpClusterer();
               // SetUplocation();








            }
        });


        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.onResume();




    }


    public void setUpClusterer() {


        markers = new Hashtable<String, String>();

        googleMap.setOnCameraIdleListener(this);
        googleMap.setOnMarkerClickListener(this);
        googleMap.setOnInfoWindowClickListener(this);

        try {
            googleMap.setInfoWindowAdapter(new CustomInfoWindowAdapter());

        } catch (Exception ex) {
            String df = "";
        }

        addItems();

        // BindData();


    }

    public Marker markerGlobal;
    public Hashtable<String, String> markers;
    ArrayList<Marker> markerArrayList;
    String clickedId;

    @Override
    public void onCameraIdle() {

    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        marker.showInfoWindow();
        return false;
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        //marker.showInfoWindow();
        context.startActivity(new Intent(context, RiderDetails.class)
                .putExtra(Constants.task, clickedService)
                .putExtra(Constants.latitude, latLng.latitude)
                .putExtra(Constants.longitude, latLng.longitude));

    }


    private class CustomInfoWindowAdapter implements GoogleMap.InfoWindowAdapter {

        private View view;

        public CustomInfoWindowAdapter() {
            view = getLayoutInflater().inflate(R.layout.custom_info_window,
                    null);
        }

        @Override
        public View getInfoContents(Marker marker) {

            if (marker != null
                    && marker.isInfoWindowShown()) {
                marker.hideInfoWindow();
                marker.showInfoWindow();
            }
            return null;
        }

        @Override
        public View getInfoWindow(Marker marker) {
            try {


                markerGlobal = marker;

                //Utilities.showMessage(context,"clicked");
                String url = null;


                Marker temp = markerArrayList.get(markerArrayList.indexOf(markerGlobal));

                clickedId = String.valueOf(temp.getTag());
                url = markers.get(clickedId);

                SearchAndSetClickedService();
            /*if (marker.getId() != null && markers != null && markers.size() > 0) {
                if ( markers.get(marker.getId()) != null &&
                        markers.get(marker.getId()) != null) {
                    url = markers.get(marker.getId());
                }
            }*/
                final ImageView image = ((ImageView) view.findViewById(R.id.badge));

                if (url != null && !url.equalsIgnoreCase("null")
                        && !url.equalsIgnoreCase("")) {

                    Glide.with(context)
                            .asBitmap()
                            .load("")
                            .placeholder(R.drawable.ic_user)
                            .centerCrop()
                            .into(new BitmapImageViewTarget(image) {
                                @Override
                                protected void setResource(Bitmap resource) {
                                    RoundedBitmapDrawable circularBitmapDrawable =
                                            RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                                    circularBitmapDrawable.setCornerRadius(4.0f);


                                    image.setImageDrawable(circularBitmapDrawable);
                                }
                            });


                } else {
                    image.setImageResource(R.drawable.ic_user);
                }

                final String title = marker.getTitle();
                final TextView titleUi = ((TextView) view.findViewById(R.id.title));
                if (title != null) {
                    titleUi.setText(title);
                } else {
                    titleUi.setText("");
                }

            /*final String snippet = marker.getSnippet();
            final TextView snippetUi = ((TextView) view
                    .findViewById(R.id.snippet));
            if (snippet != null) {
                snippetUi.setText(snippet);
            } else {
                snippetUi.setText("");
            }*/

            } catch (Exception ex) {

            }
            return view;
        }
    }


    Nearby_riders clickedService;

    private void SearchAndSetClickedService() {
        for (int i = 0; i < nearByRider.getNearby_riders().length; i++) {
            if (nearByRider.getNearby_riders()[i].getId().equalsIgnoreCase(clickedId)) {
                clickedService = nearByRider.getNearby_riders()[i];
            }
        }
    }


    @SuppressLint("MissingPermission")
    private void addItems() {

        try {


            LatLngBounds.Builder builder = new LatLngBounds.Builder();

            if (nearByRider != null) {

                markerArrayList = new ArrayList<>();
                //mClusterManager.clearItems();
                googleMap.clear();
                if (latLng != null) {

                    builder.include(latLng);
                }
                for (int i = 0; i < nearByRider.getNearby_riders().length; i++) {


                    markers.put(nearByRider.getNearby_riders()[i].getId(), nearByRider.getNearby_riders()[i].getUserImage());

                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLng(Double.parseDouble(nearByRider.getNearby_riders()[i].getLat()),
                                    Double.parseDouble(nearByRider.getNearby_riders()[i].getLon())))
                            .title(nearByRider.getNearby_riders()[i].getUsername())
                            //.snippet("")
                            .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));


                    Marker tr = googleMap.addMarker(markerOptions);
                    tr.setTag(String.valueOf(nearByRider.getNearby_riders()[i].getId()));
                    markerArrayList.add(tr);

                    builder.include(new LatLng(Double.parseDouble(nearByRider.getNearby_riders()[i].getLat())
                            , Double.parseDouble(nearByRider.getNearby_riders()[i].getLon())));

                }

                LatLngBounds bounds = builder.build();


                if (builder != null && bounds != null) {

                    int width = getResources().getDisplayMetrics().widthPixels;
                    width = (int) (width * 0.30);

                    int height = getResources().getDisplayMetrics().heightPixels;
                    height = (int) (height * 0.30);

                    int padding = (int) (width * 0.2); // offset from edges of the map 10% of screen

                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
                    //googleMap.animateCamera(cu);

                }
                //googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(bounds.getCenter(), 0));

                try {
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds,
                            AppHelper.dpToPx(context, 155));
                    googleMap.animateCamera(cu);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                //googleMap.

            }


        } catch (Exception ex) {

            String df = "";
        }


    }




    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();




    }


    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }




}
