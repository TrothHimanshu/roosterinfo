package com.trothmatrix.roosterinfo.Model;

import java.io.Serializable;

public class User implements Serializable {

    private String name;

    private String id;

    private String mobileno;

    private String deviceid;

    private String idref;

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMobileno ()
    {
        return mobileno;
    }

    public void setMobileno (String mobileno)
    {
        this.mobileno = mobileno;
    }

    public String getDeviceid ()
    {
        return deviceid;
    }

    public void setDeviceid (String deviceid)
    {
        this.deviceid = deviceid;
    }

    public String getIdref ()
    {
        return idref;
    }

    public void setIdref (String idref)
    {
        this.idref = idref;
    }



}
