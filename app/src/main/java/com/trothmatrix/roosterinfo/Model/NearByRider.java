package com.trothmatrix.roosterinfo.Model;

import java.io.Serializable;

public class NearByRider implements Serializable {
    private String latitude;

    private Nearby_riders[] nearby_riders;

    private String name;

    //private Object past_route_array;

    private String id;

    private String mobileno;

    private String deviceid;

    private String idref;

    private String longitude;

    private String status;

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public Nearby_riders[] getNearby_riders ()
    {
        return nearby_riders;
    }

    public void setNearby_riders (Nearby_riders[] nearby_riders)
    {
        this.nearby_riders = nearby_riders;
    }

    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }



    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMobileno ()
    {
        return mobileno;
    }

    public void setMobileno (String mobileno)
    {
        this.mobileno = mobileno;
    }

    public String getDeviceid ()
    {
        return deviceid;
    }

    public void setDeviceid (String deviceid)
    {
        this.deviceid = deviceid;
    }

    public String getIdref ()
    {
        return idref;
    }

    public void setIdref (String idref)
    {
        this.idref = idref;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }
}
