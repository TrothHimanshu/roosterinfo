package com.trothmatrix.roosterinfo.Model;

public class RiderDetail {

    private String distance;

    private String image_url;

    private String latitude;

    private Past_route_array[] past_route_array;

    private String mobileno;

    private String deviceid;

    private String idref;

    private String frequency;

    //private null nearby_riders;

    private String name;

    private String id;

    private String longitude;

    private String status;

    public String getDistance ()
    {
        return distance;
    }

    public void setDistance (String distance)
    {
        this.distance = distance;
    }

    public String getImage_url ()
    {
        return image_url;
    }

    public void setImage_url (String image_url)
    {
        this.image_url = image_url;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public Past_route_array[] getPast_route_array ()
    {
        return past_route_array;
    }

    public void setPast_route_array (Past_route_array[] past_route_array)
    {
        this.past_route_array = past_route_array;
    }

    public String getMobileno ()
    {
        return mobileno;
    }

    public void setMobileno (String mobileno)
    {
        this.mobileno = mobileno;
    }

    public String getDeviceid ()
    {
        return deviceid;
    }

    public void setDeviceid (String deviceid)
    {
        this.deviceid = deviceid;
    }

    public String getIdref ()
    {
        return idref;
    }

    public void setIdref (String idref)
    {
        this.idref = idref;
    }

    public String getFrequency ()
    {
        return frequency;
    }

    public void setFrequency (String frequency)
    {
        this.frequency = frequency;
    }



    public String getName ()
    {
        return name;
    }

    public void setName (String name)
    {
        this.name = name;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }


}
