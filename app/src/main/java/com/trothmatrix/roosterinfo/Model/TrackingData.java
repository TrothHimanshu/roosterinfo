package com.trothmatrix.roosterinfo.Model;

public class TrackingData {

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }

    private String distance;

    private String datetime;

    private String bearing;

    private String latitude;

    private String deviceid;

    private String speed;

    private String longitude;

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    private String vin;


    public String getDatetime ()
    {
        return datetime;
    }

    public void setDatetime (String datetime)
    {
        this.datetime = datetime;
    }

    public String getBearing ()
    {
        return bearing;
    }

    public void setBearing (String bearing)
    {
        this.bearing = bearing;
    }

    public String getLatitude ()
    {
        return latitude;
    }

    public void setLatitude (String latitude)
    {
        this.latitude = latitude;
    }

    public String getDeviceid ()
    {
        return deviceid;
    }

    public void setDeviceid (String deviceid)
    {
        this.deviceid = deviceid;
    }

    public String getSpeed ()
    {
        return speed;
    }

    public void setSpeed (String speed)
    {
        this.speed = speed;
    }

    public String getLongitude ()
    {
        return longitude;
    }

    public void setLongitude (String longitude)
    {
        this.longitude = longitude;
    }

}
