package com.trothmatrix.roosterinfo.Model;

public class Past_route_array
{
    private String lon;

    private String lat;


    public TrackingData trackingData = new TrackingData();


    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [lon = "+lon+", lat = "+lat+"]";
    }
}
			