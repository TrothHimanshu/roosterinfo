package com.trothmatrix.roosterinfo.Model;

import java.io.Serializable;

public class Nearby_riders implements Serializable
{
    private String userImage;

    private String lon;

    private String id;

    private String mobileno;

    private String lat;

    private String username;

    public String getUserImage ()
    {
        return userImage;
    }

    public void setUserImage (String userImage)
    {
        this.userImage = userImage;
    }

    public String getLon ()
    {
        return lon;
    }

    public void setLon (String lon)
    {
        this.lon = lon;
    }

    public String getId ()
    {
        return id;
    }

    public void setId (String id)
    {
        this.id = id;
    }

    public String getMobileno ()
    {
        return mobileno;
    }

    public void setMobileno (String mobileno)
    {
        this.mobileno = mobileno;
    }

    public String getLat ()
    {
        return lat;
    }

    public void setLat (String lat)
    {
        this.lat = lat;
    }

    public String getUsername ()
    {
        return username;
    }

    public void setUsername (String username)
    {
        this.username = username;
    }


}