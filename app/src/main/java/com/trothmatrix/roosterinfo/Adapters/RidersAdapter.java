package com.trothmatrix.roosterinfo.Adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;
import com.trothmatrix.roosterinfo.Classes.AppHelper;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.GlideHelper;
import com.trothmatrix.roosterinfo.Model.NearByRider;
import com.trothmatrix.roosterinfo.Model.Nearby_riders;
import com.trothmatrix.roosterinfo.R;
import com.trothmatrix.roosterinfo.RiderDetails;

import java.util.List;


public class RidersAdapter extends RecyclerView.Adapter<RidersAdapter.ProductViewHolder> {

    NearByRider riders;
    Context context;
   LatLng  latLng;


    public RidersAdapter(Context context, NearByRider riders, LatLng latLng) {
        this.context = context;
        this.riders = riders;
        this.latLng = latLng;

    }


    public class ProductViewHolder extends RecyclerView.ViewHolder {
        public TextView nameTextView,distanceTextView,phoneTextView;


        ImageView riderImageView;



        private ProductViewHolder(View view) {
            super(view);

            nameTextView = view.findViewById(R.id.nameTextView);

            distanceTextView = view.findViewById(R.id.distanceTextView);

            phoneTextView = view.findViewById(R.id.phoneTextView);


            riderImageView = view.findViewById(R.id.riderImageView);

        }


    }

    @Override
    public ProductViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {

        return new ProductViewHolder(
                LayoutInflater.from(viewGroup.getContext())
                        .inflate(R.layout.layout_item_rider, viewGroup, false));
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(final ProductViewHolder holder, final int position)
    {

        Nearby_riders nearbyRider = riders.getNearby_riders()[position];

        holder.nameTextView.setText(nearbyRider.getUsername());
        holder.phoneTextView.setText(nearbyRider.getMobileno());
        holder.distanceTextView.setText(AppHelper.getDistance(latLng,nearbyRider.getLat(),nearbyRider.getLon()) + " km");

        GlideHelper.LoadCircularImageMethod1(context,holder.riderImageView,R.drawable.ic_user,nearbyRider.getUserImage());
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                context.startActivity(new Intent(context, RiderDetails.class)
                .putExtra(Constants.task,riders.getNearby_riders()[position])
                .putExtra(Constants.latitude,latLng.latitude)
                        .putExtra(Constants.longitude,latLng.longitude)

                );

            }
        });

    }

    @Override
    public int getItemCount() {

        return riders == null ? 0 : riders.getNearby_riders().length
                //riders.size()
                ;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }



}

