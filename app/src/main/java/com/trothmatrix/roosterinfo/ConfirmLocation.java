package com.trothmatrix.roosterinfo;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.trothmatrix.roosterinfo.Classes.Constants;
import com.trothmatrix.roosterinfo.Classes.DefaultAlertPopUp;
import com.trothmatrix.roosterinfo.Classes.OkButtonPopUpInterface;
import com.trothmatrix.roosterinfo.Classes.ToastMessage;
import com.trothmatrix.roosterinfo.LoginSignUp.Login;

import java.util.List;

import static android.Manifest.permission.ACCESS_FINE_LOCATION;

public class ConfirmLocation extends AppCompatActivity {

    Context context;

    com.trothmatrix.roosterinfo.databinding.ActivityConfirmLocationBinding binding;

    String mode = Constants.mode.car;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirm_location);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_confirm_location);


        context = this;

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
        InitializeMaps(savedInstanceState);


        SetUpRadioGroup();


        binding.confirmButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(latLng==null)
                {
                    return;
                }

                startActivity(new Intent(context,Home.class)
                .putExtra(Constants.latitude,latLng.latitude)
                        .putExtra(Constants.longitude,latLng.longitude));

                finish();


            }
        });


    }

    public void carSelected()
    {
        binding.car.setBackgroundResource(R.drawable.shape_rectangle_left_round_solid_red);
        setTextWhite(binding.car);
        setWhiteTint(binding.car);

        binding.bike.setBackgroundResource(R.drawable.shape_rectangle_centre_stroke_grey);
        setTextBlack(binding.bike);
        setBlackTint(binding.bike);

        binding.walking.setBackgroundResource(R.drawable.shape_rectangle_right_right_stroke_grey);
        setTextBlack(binding.walking);
        setBlackTint(binding.walking);

        mode = Constants.mode.car;
    }

    public void bikeSelected()
    {
        binding.car.setBackgroundResource(R.drawable.shape_rectangle_right_left_stroke_grey);
        setTextBlack(binding.car);
        setBlackTint(binding.car);

        binding.bike.setBackgroundResource(R.drawable.shape_rectangle_centre_solid_red);
        setTextWhite(binding.bike);
        setWhiteTint(binding.bike);

        binding.walking.setBackgroundResource(R.drawable.shape_rectangle_right_right_stroke_grey);
        setTextBlack(binding.walking);
        setBlackTint(binding.walking);

        mode = Constants.mode.bike;
    }

    public void walkingSelected()
    {
        binding.car.setBackgroundResource(R.drawable.shape_rectangle_right_left_stroke_grey);
        setTextBlack(binding.car);
        setBlackTint(binding.car);

        binding.bike.setBackgroundResource(R.drawable.shape_rectangle_centre_stroke_grey);
        setTextBlack(binding.bike);
        setBlackTint(binding.bike);

        binding.walking.setBackgroundResource(R.drawable.shape_rectangle_right_round_solid_red);
        setTextWhite(binding.walking);
        setWhiteTint(binding.walking);

        mode = Constants.mode.walking;
    }

    private void setTextViewDrawableColor(TextView textView, int color) {
        for (Drawable drawable : textView.getCompoundDrawables()) {
            if (drawable != null) {
                drawable.setColorFilter(new PorterDuffColorFilter(ContextCompat.getColor(textView.getContext(), color), PorterDuff.Mode.SRC_IN));
            }
        }
    }
    public void setWhiteTint(TextView textView)
    {
       setTextViewDrawableColor(textView,R.color.white);

    }

    public void setBlackTint(TextView textView)
    {
        setTextViewDrawableColor(textView,R.color.black);

    }

    public void setTextWhite(TextView textView)
    {
        textView.setTextColor(context.getResources().getColor(R.color.white));

    }

    public void setTextBlack(TextView textView)
    {
        textView.setTextColor(context.getResources().getColor(R.color.black));

    }



    private void SetUpRadioGroup() {



        binding.car.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {
                    carSelected();
                }
            }
        });

        binding.bike.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {

                    bikeSelected();
                }
            }
        });

        binding.walking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b)
                {

                    walkingSelected();
                }
            }
        });



    }

    GoogleMap googleMap;
    private void InitializeMaps(Bundle savedInstanceState) {

        binding.mapview.getMapAsync(new OnMapReadyCallback() {
            @SuppressLint("MissingPermission")
            @Override
            public void onMapReady(GoogleMap mMap) {
                googleMap = mMap;


                SetUplocation();





            }
        });


        binding.mapview.onCreate(savedInstanceState);
        binding.mapview.onResume();

    }










    // Map Code





    public Boolean isGpsEnabled() {
        LocationManager manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        return manager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }
    private void SetUplocation() {

        if (request_permission()) {

            if (!isGpsEnabled()) {
                DefaultAlertPopUp.OkButtonAlertPopUp(context, "GPS Disabled!",
                        "Enable GPS to get current location.",
                        "Ok",
                        true, R.color.blue, new OkButtonPopUpInterface() {
                            @Override
                            public void onClickOkButton() {

                                isEnableGPSClicked = true;
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
                            }
                        });

                return;
            }

            getLocationUpdates();
        }


    }

    private LocationRequest locationRequest;
    public Location location;
    public LatLng latLng;
    private FusedLocationProviderClient fusedLocationClient;

    @SuppressLint("MissingPermission")
    public void getLocationUpdates() {
        locationRequest = LocationRequest.create();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(20 * 1000);

        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, getMainLooper());
    }
    private LocationCallback locationCallback = new LocationCallback() {
        @Override
        public void onLocationResult(LocationResult locationResult) {
            if (locationResult == null) {
                return;
            }
            fusedLocationClient.removeLocationUpdates(locationCallback);
            Log.e("got location", "loc");

            List<Location> locationList = locationResult.getLocations();
            location = locationList.get(0);


            if (location != null) {
                if (fusedLocationClient != null) {

                    latLng = new LatLng(location.getLatitude(), location.getLongitude());


                    // googleMap.getCameraPosition().
                    googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                    googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
                    googleMap.addMarker(new MarkerOptions().position(latLng));


                }

            }

        }


    };



    private boolean request_permission() {


        if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
            //|| ContextCompat.checkSelfPermission(context, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
        ) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(new String[]{
                        // Manifest.permission.CAMERA,
                        Manifest.permission.ACCESS_COARSE_LOCATION,
                        ACCESS_FINE_LOCATION,
                        //Manifest.permission.READ_PHONE_STATE,
                        //Manifest.permission.READ_EXTERNAL_STORAGE,
                        //Manifest.permission.WRITE_EXTERNAL_STORAGE
                }, 5);
            }
            return false;
        } else {
            return true;
        }


    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == 5)
            if (ContextCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(context, ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {

                SetUplocation();

            } else {

                ToastMessage.showMessage(context, "Permission denied. please enable permission..");

            }


    }


    boolean isEnableGPSClicked = false;

    @Override
    public void onResume() {
        super.onResume();
        binding.mapview.onResume();

        if (isEnableGPSClicked) {
            isEnableGPSClicked = false;
            SetUplocation();
        }


    }


    @Override
    public void onPause() {
        super.onPause();
        binding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapview.onLowMemory();
    }



}
