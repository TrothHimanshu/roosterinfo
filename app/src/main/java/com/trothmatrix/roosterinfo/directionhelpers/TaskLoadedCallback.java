package com.trothmatrix.roosterinfo.directionhelpers;

/**
 */

public interface TaskLoadedCallback {
    void onTaskDone(Object... values);
}
