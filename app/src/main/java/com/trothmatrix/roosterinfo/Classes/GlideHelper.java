package com.trothmatrix.roosterinfo.Classes;

import android.content.Context;
import android.graphics.Bitmap;
import android.widget.ImageView;

import androidx.core.graphics.drawable.RoundedBitmapDrawable;
import androidx.core.graphics.drawable.RoundedBitmapDrawableFactory;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.trothmatrix.roosterinfo.R;


public class GlideHelper {


    public static void LoadImageSimpleNoPlaceHolder(Context context, String url, ImageView imageView) {

        if (context != null) {
            Glide.with(context.getApplicationContext()).load(url)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.skipMemoryCache(true)
                    .into(imageView);
        }
    }

    public static void LoadImageSimple(Context context, String url, int placeHolder, ImageView imageView) {

        if (context != null) {
            Glide.with(context.getApplicationContext()).load(url)
                    .centerCrop()
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //.skipMemoryCache(true)
                    .placeholder(placeHolder)
                    .into(imageView);

        }
    }



    public static void LoadCircularImageMethod1(final Context mContext, final ImageView imageView,
                                                final int placeholder, String url) {
        Glide.with(mContext)
                .asBitmap()
                .load(url)
                .placeholder(placeholder)
                .into(new BitmapImageViewTarget(imageView) {
                    @Override
                    protected void setResource(Bitmap resource) {
                        RoundedBitmapDrawable circularBitmapDrawable =
                                RoundedBitmapDrawableFactory.create(mContext.getResources(), resource);
                        circularBitmapDrawable.setCircular(true);
                        imageView.setImageDrawable(circularBitmapDrawable);
                    }
                });





    }


    public static void LoadCircularImageMethod2(Context context, String url, int placeholder, ImageView imageView) {
        Glide.with(context)
                .load(url)
                //.transform(new CircleTransform(context))
                .placeholder(placeholder)
                .into(imageView);

    }




    public static void LoadImageOuterLayoutZero(Context context, String url, int placeHolder, ImageView imageView) {

        imageView.layout(0, 0, 0, 0);
        if (context != null) {
            Glide.with(context.getApplicationContext()).load(url)
                    .fitCenter()
                    //.diskCacheStrategy(DiskCacheStrategy.NONE)
                    //.skipMemoryCache(true)
                    .placeholder(placeHolder)
                    .into(imageView);
        }
    }



    /*public static void LoadLocalRoundCornerImage(Context context, byte[] bytes, ImageView imageView) {

        if (context != null) {
            Glide.with(context)
                    .bitmapTransform(new RoundedCornersTransformation(context,
                            context.getResources().getDimensionPixelOffset(R.dimen.dp_12), 0,
                            RoundedCornersTransformation.CornerType.ALL))
                    .load(bytes)

                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);
        }
    }*/

    public static void LoadLocalCircularImage(final Context context, byte[] bytes, final ImageView imageView) {

        if (context != null) {

            /*Glide.with(context).load(bytes)
                    .bitmapTransform(new RoundedCornersTransformation(context,
                            context.getResources().getDimensionPixelOffset(R.dimen.dp_12), 0,
                            RoundedCornersTransformation.CornerType.ALL))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);*/


            Glide.with(context)
                    .asBitmap()
                    .load(bytes)
                    .centerCrop()
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });


        }
    }

    public static void LoadLocalCircularImageInCircularImageView(final Context context,
                                                                 String url, int placeHolder,
                                                                 final com.makeramen.roundedimageview.RoundedImageView imageView) {

        if (context != null) {

            /*Glide.with(context).load(bytes)
                    .bitmapTransform(new RoundedCornersTransformation(context,
                            context.getResources().getDimensionPixelOffset(R.dimen.dp_12), 0,
                            RoundedCornersTransformation.CornerType.ALL))
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .skipMemoryCache(true)
                    .into(imageView);*/


            Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .centerCrop()
                    .placeholder(placeHolder)
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {
                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCircular(true);
                            imageView.setImageDrawable(circularBitmapDrawable);
                        }
                    });


        }
    }


    public static void LoadRoundCornerImageMethod1(final Context context, String url, int placeHolder, final ImageView imageView) {

        if (context != null) {


            Glide.with(context)
                    .asBitmap()
                    .load(url)
                    .placeholder(placeHolder)
                    .centerCrop()
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    .into(new BitmapImageViewTarget(imageView) {
                        @Override
                        protected void setResource(Bitmap resource) {

                            RoundedBitmapDrawable circularBitmapDrawable =
                                    RoundedBitmapDrawableFactory.create(context.getResources(), resource);
                            circularBitmapDrawable.setCornerRadius(12.0f);
                            imageView.setImageDrawable(circularBitmapDrawable);

                        }
                    });


        }
    }

    /*public static void LoadRoundCornerImageMethod2(Context context, String url, int placeHolder, ImageView imageView) {

        if (context != null) {
            Glide.with(context)

                    .load(url)
                    .bitmapTransform(new RoundedCornersTransformation(context,
                            context.getResources().getDimensionPixelOffset(R.dimen.dp_12), 0,
                            RoundedCornersTransformation.CornerType.ALL)).placeholder(placeHolder)
                    .diskCacheStrategy(DiskCacheStrategy.ALL)
                    //.skipMemoryCache(true)
                    //.centerCrop()
                    .into(imageView);





        }
    }

    */


}
