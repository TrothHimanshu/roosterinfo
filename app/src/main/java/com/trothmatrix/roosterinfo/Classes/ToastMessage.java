package com.trothmatrix.roosterinfo.Classes;

import android.content.Context;
import android.view.Gravity;
import android.widget.Toast;

public class ToastMessage {
    public static Toast toast;

    public static void showMessage(Context context, String msg) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.show();
    }

    public static void showMessageCenter(Context context, String msg) {
        if (toast != null) {
            toast.cancel();
        }

        toast = Toast.makeText(context, msg, Toast.LENGTH_LONG);
        toast.setGravity(Gravity.CENTER, 0, 0);
        toast.show();
    }
}
