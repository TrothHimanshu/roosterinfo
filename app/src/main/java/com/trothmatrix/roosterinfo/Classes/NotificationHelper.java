package com.trothmatrix.roosterinfo.Classes;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Build;
import android.provider.Settings;

import androidx.core.content.ContextCompat;

import com.trothmatrix.roosterinfo.Home;
import com.trothmatrix.roosterinfo.R;


public class NotificationHelper {

    private Context mContext;
    private NotificationManager mNotificationManager;
    private Notification.Builder mBuilder;
    public static final String NOTIFICATION_CHANNEL_ID = "10001";

    public NotificationHelper(Context context) {
        mContext = context;
    }




    public void createTrackingNotification(String title, String message, String type) {
        /**Creates an explicit intent for an Activity in your app**/
        Intent resultIntent = new Intent(mContext, Home.class);
        //resultIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

        createNotification(title,message,resultIntent);

    }





    public void createNotification(String title, String message, Intent resultIntent)
    {
        PendingIntent resultPendingIntent = PendingIntent.getActivity(mContext,
                0 /* Request code */, resultIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder = new Notification.Builder(mContext);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            mBuilder
                    //.setSmallIcon(getNotificationIcon())
                    .setSmallIcon(R.drawable.icon_rooster_logo)
                    .setColor(ContextCompat.getColor(mContext, R.color.colorPrimary))
                    .setLargeIcon(BitmapFactory.decodeResource(mContext.getResources(), R.mipmap.ic_launcher));
            //.setLargeIcon(R.drawable.app_logo);

        }

        mBuilder.setContentTitle(title)
                .setContentText(message)
                .setAutoCancel(false)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setContentIntent(resultPendingIntent);

        mNotificationManager = (NotificationManager) mContext.getSystemService(Context.NOTIFICATION_SERVICE);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            int importance = NotificationManager.IMPORTANCE_HIGH;
            NotificationChannel notificationChannel = new NotificationChannel(NOTIFICATION_CHANNEL_ID, "NOTIFICATION_CHANNEL_NAME", importance);
            notificationChannel.enableLights(true);
            notificationChannel.setLightColor(Color.RED);
            notificationChannel.enableVibration(true);
            notificationChannel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
            assert mNotificationManager != null;
            mBuilder.setChannelId(NOTIFICATION_CHANNEL_ID);
            mNotificationManager.createNotificationChannel(notificationChannel);
        }
        assert mNotificationManager != null;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
            mNotificationManager.notify(0 /* Request Code */, mBuilder.build());
        }
    }

    /*private int getNotificationIcon() {
        boolean useWhiteIcon = (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP);
        return useWhiteIcon ? R.drawable.ser : R.mipmap.ic_launcher;
    }*/


    /*public void saveNotification(RemoteMessage remoteMessage) {
        //@SuppressLint({"NewApi", "LocalSuppress"}) ArrayMap<String,String> notificationMap =  (ArrayMap<String,String>)message.getData();
        NotificationsOffline.addNotificationsOffline(mContext,String.valueOf(remoteMessage.getSentTime())
                , remoteMessage.getNotification().getTitle()
                , remoteMessage.getNotification().getBody()
                , String.valueOf(remoteMessage.getSentTime()));

    }

    public static class NotificationsOffline {

        private static String NotificationsOfflineFolder = "Notifications Offline Folder";

        private static String NotificationsOfflineFile = "Notifications Offline File";

        public static void addNotificationsOffline(Context context,String id, String title, String msg, String date) {
            try {

                JSONObject jsonObject = new JSONObject();
                jsonObject.put("title",title);
                jsonObject.put("msg",msg);
                jsonObject.put("date",date);

                JSONArray jsonArray = new JSONArray();
                jsonArray = getNotificationsOfflineJSONArray(context);

                if(jsonArray == null)
                {
                    jsonArray = new JSONArray();
                }

                jsonArray.put(jsonObject);


                String jsonStr = jsonArray.toString();


                FileHelper.WriteAllText(context,jsonStr,NotificationsOfflineFolder,NotificationsOfflineFile);



            } catch (Exception ex) {
                String ss = ex.getMessage();
            }

        }




        public static JSONArray getNotificationsOfflineJSONArray(Context context) {
            try
            {

               String jsonStr = FileHelper.ReadAllText(context,NotificationsOfflineFolder,NotificationsOfflineFile);

               if(jsonStr==null)
               {
                   return  null;
               }
               JSONArray jsonArray = new JSONArray(jsonStr);
               return jsonArray;

            } catch (Exception ex) {
                String exm = ex.getMessage();
            }
            return null;

        }

        public static int getNotificationCount(Context context)
        {

            JSONArray jsonArray = getNotificationsOfflineJSONArray(context);
            if(jsonArray==null)
            {
                return 0;
            }
            else
            {
                return jsonArray.length();
            }
        }


        public static void removeNotificationsOffline(Context context) {

            //FileInputStream inputStream = context.openFileInput(ImagesOfflineFile +".properties");

            FileHelper.Delete(context,NotificationsOfflineFolder,NotificationsOfflineFile);
            //File file = new File(context.getFilesDir().getPath().toString() + "/" + NotificationsOfflineFile + ".properties");

            //if (file.exists()) {
             //   file.delete();
            //}


        }

    }*/
}