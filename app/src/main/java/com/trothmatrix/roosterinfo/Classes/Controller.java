package com.trothmatrix.roosterinfo.Classes;

import android.content.Context;
import android.content.SharedPreferences;


public class Controller {






    public static class CurrentUser
    {


        private static String LoginInStatusKey = "LoginInStatus Key";

        private static String EmailIdKey = "Email Id";
        private static String IdKey = "User Id";
        private static String NameKey = "Name";
        private static String RefCodeKey = "RefCode";
        private static String PhoneKey = "Phone";
        private static String PINCodeKey = "PINCode";
        private static String isPINValidKey = "isPINValid";
        private static String isPINVerifiedKey = "isPINVerified";

        private static String FirstNameKey = "FirstName";
        private static String LastNameKey = "LastName";
        private static String ImageKey = "Image";

        private static String AccountTypeKey = "Sinch User Id";
        private static String LocationKey = "LocationKey";



        private static String UserDataSharedPref = "User Data v1";





        public static void setImage(Context context, String role) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(ImageKey, role);
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static String getImage(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(ImageKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }




        public static void setName(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(NameKey, name);
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static String getName(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(NameKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }




        public static void setFirstName(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(FirstNameKey, name);
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static String getPINCode(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(PINCodeKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }


        public static void setPINCode(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(PINCodeKey, name);
                editor.apply();

            } catch (Exception ex) {

            }


        }


        ///


        public static Boolean isPINVerified(Context context) {
            Boolean logged = false;
            SharedPreferences sharedPref = context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE);
            if (sharedPref.contains(isPINVerifiedKey)) {
                if (sharedPref.getString(isPINVerifiedKey, "").equals("true")) {
                    logged = true;
                } else {
                    logged = false;
                }
            }
            return logged;
        }

        public static void setPINVerified(Context context) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(isPINVerifiedKey, "true");
                editor.apply();

            } catch (Exception ex) {

            }

        }



        ///




        public static Boolean isPINValid(Context context) {
            Boolean logged = false;
            SharedPreferences sharedPref = context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE);
            if (sharedPref.contains(isPINValidKey)) {
                if (sharedPref.getString(isPINValidKey, "").equals("true")) {
                    logged = true;
                } else {
                    logged = false;
                }
            }
            return logged;
        }

        public static void setPINValid(Context context) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(isPINValidKey, "true");
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static void setPINInValid(Context context) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(isPINValidKey, "false");
                editor.apply();

            } catch (Exception ex) {

            }

        }



        public static String getPhone(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(PhoneKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }


        public static void setPhone(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(PhoneKey, name);
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static String getIdRef(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(LocationKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }


        public static void setIdRef(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(LocationKey, name);
                editor.apply();

            } catch (Exception ex) {

            }

        }



        public static String getFirstName(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(FirstNameKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }


        public static void setLastName(Context context, String name) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(LastNameKey, name);
                editor.apply();

            } catch (Exception ex) {

            }

        }

        public static String getLastName(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(LastNameKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }









        public static Boolean isUserLoggedIn(Context context) {
            Boolean logged = false;
            SharedPreferences sharedPref = context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE);
            if (sharedPref.contains(LoginInStatusKey)) {
                if (sharedPref.getString(LoginInStatusKey, "").equals("true")) {
                    logged = true;
                } else {
                    logged = false;
                }
            }
            return logged;
        }

        public static void setUserId(Context context, String id) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(IdKey, id);
                editor.apply();

            } catch (Exception ex) {

            }
        }

        public static String AccountTypeFacebook = "facebook";
        public static String AccountTypeServer = "server";

        public static void setAccountType(Context context, String id) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(AccountTypeKey, id);
                editor.apply();

            } catch (Exception ex) {

            }
        }

        public static boolean isAccountTypeServer(Context context) {

           boolean accountTypeServer = false;
            try {

                if(context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(AccountTypeKey, "").equalsIgnoreCase(AccountTypeServer))
                {
                    accountTypeServer = true;
                }
            } catch (Exception ex) {
                // Ignore.
            }

            return accountTypeServer;
        }



        public static void setUserLoggedIn(Context context) {
            try {



                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(LoginInStatusKey, "true");
                editor.apply();

            } catch (Exception ex) {

            }

        }


        public static String getUserId(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(IdKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }

        public static void setDeviceId(Context context, String email) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(RefCodeKey, email);
                editor.apply();

            } catch (Exception ex) {

            }

        }



        public static String getDeviceId(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(RefCodeKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }




        public static void setEmailId(Context context, String email) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(EmailIdKey, email);
                editor.apply();

            } catch (Exception ex) {

            }

        }



        public static String getEmailId(Context context) {
            try {
                return context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE)
                        .getString(EmailIdKey, "");
            } catch (Exception ex) {
                // Ignore.
            }

            return null;
        }

        public static void setUserLoggedOut(Context context) {
            try {
                SharedPreferences.Editor editor =
                        context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit();
                editor.putString(LoginInStatusKey, "false");
                editor.apply();

            } catch (Exception ex) {

            }

        }





        public static void LogoutUser(Context context) {

            //ProgressDialog pd = new ProgressDialog(context);
            //pd.setMessage("logging out");
            //pd.show();


            setUserLoggedOut(context);
            DeleteAllData(context);
            //SettingsData.DeleteSettingsData(context);
            // pd.dismiss();

            //Intent intent = new Intent(context, FirebaseEmailAuthentication.class);
            //context.startActivity(intent);
        }

        public static void DeleteAllData(Context context) {
            try {

                DeleteAllSharedPref(context);
                //DeleteCachedData(context);

                //DeleteUserJsonFile(context);

            } catch (Exception ex) {
                // Ignore.
            }
        }

        private static void DeleteAllSharedPref(Context context) {
            try {
                DeleteUserDataSharedPref(context);

            } catch (Exception ex) {
                // Ignore.
            }
        }

        private static void DeleteUserDataSharedPref(Context context) {
            try {
                context.getSharedPreferences(UserDataSharedPref, Context.MODE_PRIVATE).edit().
                        clear().apply();
            } catch (Exception ex) {
                //Ignore
            }
        }


    }

}
