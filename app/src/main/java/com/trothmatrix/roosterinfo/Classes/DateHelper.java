package com.trothmatrix.roosterinfo.Classes;


import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.TimeUnit;

import static java.util.concurrent.TimeUnit.MILLISECONDS;


public class DateHelper {


    public static String ConvertTimeFromOneTimeZoneToSecond(String dateString,String oneTimeZoneString) {

        try {
            SimpleDateFormat input = new SimpleDateFormat("yyyy-MM-dd hh:mm a", Locale.ENGLISH);

            input.setTimeZone(TimeZone.getTimeZone(oneTimeZoneString));
            //input.setTimeZone(TimeZone.getTimeZone("UTC")); example
            Date date = input.parse(dateString);


            SimpleDateFormat output = new SimpleDateFormat("hh:mm a", Locale.ENGLISH);
            output.setTimeZone(TimeZone.getDefault());


            return output.format(date);
        } catch (Exception ex) {



        }

        return null;

    }




    public static String CurrentTimeInMilliSec()
    {
        return  String.valueOf(System.currentTimeMillis());
    }

    public static String ConvertMilliSecTime(long milliSeconds, String dateFormat) {
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat);

        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());

    }

    public static String Now(String format) {
        return Format(Now(), format);
    }

    public static String Format(Date date, String format) {
        return new SimpleDateFormat(format, Locale.getDefault()).format(date);
    }

    public static Date Now() {
        return Calendar.getInstance().getTime();
    }

    public static String Today(String format) {
        return Format(Today(), format);
    }

    public static Date Today() {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY, 0);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);

        return c.getTime();
    }

    public static Date parse(String dateStr, String format) {
        try {
            return new SimpleDateFormat(format, Locale.getDefault()).parse(dateStr);
        } catch (Exception ex) {
            //Ignore
        }

        return null;
    }

    public static Date Set(int year, int month, int day) {
        return Set(year, month, day, 0, 0, 0);
    }

    public static Date Set(int year, int month, int day, int hrs, int min, int sec) {
        try {
            Calendar c = Calendar.getInstance();
            c.set(year, month, day, hrs, min, sec);
            return c.getTime();
        } catch (Exception ex) {
            //Ignore
        }

        return null;
    }

    public static boolean IsTimeInRange(Date time, Date startTime, Date endTime) {
        Calendar timeCal = Calendar.getInstance();
        timeCal.setTime(time);
        timeCal.set(Calendar.YEAR, 1);
        timeCal.set(Calendar.MONTH, 1);
        timeCal.set(Calendar.DAY_OF_MONTH, 1);

        Calendar startTimeCal = Calendar.getInstance();
        startTimeCal.setTime(startTime);
        startTimeCal.set(Calendar.YEAR, 1);
        startTimeCal.set(Calendar.MONTH, 1);
        startTimeCal.set(Calendar.DAY_OF_MONTH, 1);

        Calendar endTimeCal = Calendar.getInstance();
        endTimeCal.setTime(endTime);
        endTimeCal.set(Calendar.YEAR, 1);
        endTimeCal.set(Calendar.MONTH, 1);
        endTimeCal.set(Calendar.DAY_OF_MONTH, 1);

        return IsDateInRange(timeCal.getTime(), startTimeCal.getTime(), endTimeCal.getTime());
    }

    public static boolean IsDateInRange(Date date, Date startDate, Date endDate) {
        return date.after(startDate) && date.before(endDate);
    }

    public static int GetDayOfYear(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_YEAR);
    }

    public static int GetDayOfMonth(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.DAY_OF_MONTH);
    }

    public static int GetHour(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.HOUR_OF_DAY);
    }

    public static int GetMinute(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.MINUTE);
    }

    public static int GetSecond(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        return c.get(Calendar.SECOND);
    }

    //Returns the difference in milliseconds.
    public static long Subtract(Date date1, Date date2) {
        try {
            if (date1.equals(date2)) {
                return 0;
            }

            if (date1.before(date2)) {
                throw new Exception("DateHelper 1 must be greater than DateHelper 2.");
            }

            return date1.getTime() - date2.getTime();
        } catch (Exception ex) {
            //Ignore.
        }

        return -1;
    }

    private static long GetTimeZoneOffset(long time, TimeZone from, TimeZone to) {
        int fromOffset = from.getOffset(time);
        int toOffset = to.getOffset(time);
        int diff;

        if (fromOffset >= 0) {
            if (toOffset > 0) {
                toOffset = -1 * toOffset;
            } else {
                toOffset = Math.abs(toOffset);
            }
            diff = (fromOffset + toOffset) * -1;
        } else {
            if (toOffset <= 0) {
                toOffset = -1 * Math.abs(toOffset);
            }
            diff = (Math.abs(fromOffset) + toOffset);
        }
        return diff;
    }

    public static Boolean isExpired(String date, String format) {
        Boolean isExp = true;
        Date endDate = parse(date, format);
        Date nowDate = parse(Now(format), format);
        if (endDate.after(nowDate) || endDate.equals(nowDate)) {
            return false;
        }
        return isExp;
    }

    public static String UTCToLocalTime(String inputFormat, String outputFormat, String dateString)
            throws ParseException {

        SimpleDateFormat input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);

        input.setTimeZone(TimeZone.getTimeZone("UTC"));
        Date date = input.parse(dateString);


        SimpleDateFormat output = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
        output.setTimeZone(TimeZone.getDefault());
        return output.format(date);
    }

    public static String LocalToUTCTime(String inputFormat, String outputFormat, String dateString)
            throws ParseException {

        try {

            SimpleDateFormat input = new SimpleDateFormat(inputFormat, Locale.ENGLISH);

            input.setTimeZone(TimeZone.getDefault());
            Date date = input.parse(dateString);


            SimpleDateFormat output = new SimpleDateFormat(outputFormat, Locale.ENGLISH);
            output.setTimeZone(TimeZone.getTimeZone("UTC"));
            return output.format(date);

        } catch (Exception
                ex) {

        }
        return null;
    }

    public static String parseDate(String time, String inputPattern, String outputPattern) {

        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    public static double convertMilisecToSeconds(double milisec) {
        return ((milisec / (1000.0)) % 60);
    }

    public static String getTimeBasedString() {
        return (new SimpleDateFormat("ddMMyyHHmmss")).format(new Date());
    }

    public static Date GetDateinSortingFormat(String dateStr, String format) {
        Date date = null;
        if (!TextHelper.StringIsNullOrEmpty(dateStr)) {

            SimpleDateFormat inputFormat = new SimpleDateFormat(format);
            inputFormat.setTimeZone(TimeZone.getDefault());

            try {
                date = inputFormat.parse(dateStr);

            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
        return date;
    }

    public static String convertMilliSecTime(long milliSeconds, String dateFormat) {
        // Create a DateFormatter object for displaying date in specified format.
        SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.getDefault());


        // Create a calendar object that will convert the date and time value in milliseconds to date.
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliSeconds);
        return formatter.format(calendar.getTime());
    }

    public static long getCurrentTimeinMilliSecWithDecimalPoint() {
        return System.currentTimeMillis();
        //return Double.parseDouble(String.valueOf(System.currentTimeMillis()) + ".0" );

    }

    public static String getDayFullName(String day_id) {

        switch (day_id) {
            case "0":
                return "Monday";
            case "1":
                return "Tuesday";
            case "2":
                return "Wednesday";
            case "3":
                return "Thursday";
            case "4":
                return "Friday";
            case "5":
                return "Saturday";
            case "6":
                return "Sunday";
            case "7":
                return "Public holiday";
        }
        return "";
    }





    public static int getTotalMinutes(String dateTime, String format) {
        String date = Format(parse(dateTime, format), "HH:mm");
        String[] houtMin = date.split(":");
        int totalMin = (Integer.parseInt(houtMin[0]) * 60) + Integer.parseInt(houtMin[1]);
        return totalMin;
    }



    public static String GetOrderedSinceHours(String orderDate) {
        String ordersincehrstr = null;
        try {
            String d1 = orderDate.replace("T", " ");
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date orderdate = sdf.parse(d1);


            String currentDateandTime = sdf.format(new Date());

            //Calendar Calenderdate1 = Calendar.getInstance();
            //SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
            //String s = String.valueOf(Calenderdate1.getTime());
            Date currentdate = sdf.parse(currentDateandTime);
            long difference = currentdate.getTime() - orderdate.getTime();

            long secondsInMilli = 1000;
            long minutesInMilli = secondsInMilli * 60;
            long hoursInMilli = minutesInMilli * 60;
            long daysInMilli = hoursInMilli * 24;

            long elapsedDays = difference / daysInMilli;
            difference = difference % daysInMilli;

            long elapsedHours = difference / hoursInMilli;
            difference = difference % hoursInMilli;

            long elapsedMinutes = difference / minutesInMilli;
            difference = difference % minutesInMilli;

            long elapsedSeconds = difference / secondsInMilli;

            long ordersincehrs = (elapsedDays * 24) + elapsedHours;
            ordersincehrstr = String.valueOf(ordersincehrs);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return ordersincehrstr;
    }

    public static String GetOrderedSinceHoursold(String orderDate) {

        try {
            String d1 = orderDate.replace("T", " ");
            Date dateString = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(d1);
            Calendar date2 = Calendar.getInstance();
            date2.setTime(dateString);

            Calendar date1 = Calendar.getInstance(); //TimeZone.getTimeZone("GMT+5:30"
            //date1.compareTo(date2);

            long seconds = (date1.getTimeInMillis() - date2.getTimeInMillis()) / 1000;
            int hours = (int) (seconds / 3600);

            return String.valueOf(hours);


        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String GetOrderSinceHrs(String strFileDate) {
        String s = null;
        try {
            String strFileD = strFileDate.replace("T", " ");
            Date dt = new Date();
            //String strFileDate = "2012-04-20 13:10:00+5:30";
            DateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date date = formatter2.parse(strFileD);
            s = getTimeDiff(dt, date);
            //Log.i("Date is :: >>> ", s);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return s;
    }

    public static String getTimeDiff(Date dateOne, Date dateTwo) {
        String diff = "";
        long timeDiff = Math.abs(dateOne.getTime() - dateTwo.getTime());

        //diff = String.format("%d hour(s) %d min(s)", TimeUnit.MILLISECONDS.toHours(timeDiff),
        //      TimeUnit.MILLISECONDS.toMinutes(timeDiff) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(timeDiff)));

        diff = String.format("%d", MILLISECONDS.toHours(timeDiff));
        return diff;
    }

    public static class Convert {
        public static Date ToUTC(Date date, TimeZone fromTimeZone) {
            return MillisecondsToDate(date.getTime() + GetTimeZoneOffset(date.getTime(), fromTimeZone, TimeZone.getTimeZone("UTC")));
        }

        public static Date MillisecondsToDate(long milliseconds) {
            Calendar calendar = Calendar.getInstance();
            calendar.setTimeInMillis(milliseconds);
            return calendar.getTime();
        }

        public static String ToText(long totalSeconds) {
            String text = "";

            Calendar c = Calendar.getInstance();
            c.setTime(DateHelper.parse(android.text.format.DateUtils.formatElapsedTime(totalSeconds), totalSeconds >= 3600 ? "h:mm:ss" : "mm:ss"));

            if (c.get(Calendar.HOUR_OF_DAY) > 0) {
                text = text + c.get(Calendar.HOUR_OF_DAY) + " hr" + (c.get(Calendar.HOUR_OF_DAY) == 1 ? " " : "s ");
            }

            if (c.get(Calendar.MINUTE) > 0) {
                text = text + c.get(Calendar.MINUTE) + " min" + (c.get(Calendar.MINUTE) == 1 ? " " : "s ");
            }

            text = text + c.get(Calendar.SECOND) + " sec" + (c.get(Calendar.SECOND) == 1 ? " " : "s ");

            return text.trim();
        }

        public static long MillisecondsToTotalSeconds(long milliseconds) {
            return TimeUnit.MILLISECONDS.toSeconds(milliseconds);
        }

        public static long MillisecondsToTotalMinutes(long milliseconds) {
            return TimeUnit.MILLISECONDS.toMinutes(milliseconds);
        }

        public static long MillisecondsToTotalHours(long milliseconds) {
            return TimeUnit.MILLISECONDS.toHours(milliseconds);
        }

        public static long MillisecondsToTotalDays(long milliseconds) {
            return TimeUnit.MILLISECONDS.toMillis(milliseconds);
        }

        public static long SecondsToTotalMilliseconds(long seconds) {
            return TimeUnit.SECONDS.toMillis(seconds);
        }

        public static long MinutesToTotalMilliseconds(long minutes) {
            return TimeUnit.MINUTES.toMillis(minutes);
        }

        public static long HoursToTotalMilliseconds(long hours) {
            return TimeUnit.HOURS.toMillis(hours);
        }

        public static long DaysToTotalMilliseconds(long days) {
            return TimeUnit.DAYS.toMillis(days);
        }
    }




}

