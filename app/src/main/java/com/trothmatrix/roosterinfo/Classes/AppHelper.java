package com.trothmatrix.roosterinfo.Classes;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.location.Location;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.provider.Settings;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.google.android.gms.maps.model.LatLng;

import java.text.DecimalFormat;
import java.util.List;

public class AppHelper {

    public static int dpToPx(Context context,int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
    }

    public static void hideKeyboard(Activity activity, View view) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);

        if(view!=null) {
            view.clearFocus();
        }
        else
        {
            view = activity.getCurrentFocus();
        }


        if (view == null) {
            view = new View(activity);
        }
        assert imm != null;
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }


    public static String getDistance(String latitude1, String longitude1,
                                     String latitude2, String longitude2) {


        try {



        if (latitude1 != null && longitude1 != null && latitude2 != null && longitude2 != null) {


            double lat1 = Double.parseDouble(latitude1);
            double lon1 = Double.parseDouble(longitude1);

            double lat2 = Double.parseDouble(latitude2);
            double lon2 = Double.parseDouble(longitude2);

            DecimalFormat decimalFormat = new DecimalFormat("0.00");
            float[] results = new float[1];
            Location.distanceBetween(lat1, lon1, lat2, lon2, results);

            return String.valueOf(Math.round(results[0]));

        }
        }
        catch (Exception ex)
        {
           // return  "0.0";
        }

        return "0.0";
    }



    public static String getDistance2(String latitude1, String longitude1,
                                     String latitude2, String longitude2) {


        try {

            double lat1 = Double.parseDouble(latitude1);
            double lon1 = Double.parseDouble(longitude1);

            double lat2 = Double.parseDouble(latitude2);
            double lon2 = Double.parseDouble(longitude2);

            double theta = lon1 - lon2;
            double dist = Math.sin(Math.toRadians(lat1)) * Math.sin(Math.toRadians(lat2)) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1.609344 *1000;

            return  String.valueOf(Math.round(dist));



            /*if (latitude1 != null && longitude1 != null && latitude2 != null && longitude2 != null)
            {


                double lat1 = Double.parseDouble(latitude1);
                double lon1 = Double.parseDouble(longitude1);

                double lat2 = Double.parseDouble(latitude2);
                double lon2 = Double.parseDouble(longitude2);


                double pi = Math.PI;

                double R = 6371; // Radius of the earth in km
                double dLat = deg2rad(lat2-lat1);  // deg2rad below
                double dLon = deg2rad(lon2-lon1);
                double a =
                        Math.sin(dLat/2) * Math.sin(dLat/2) +
                                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                                        Math.sin(dLon/2) * Math.sin(dLon/2)
                        ;
                double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                double d = R * c; // Distance in km
                //return d;

               //double dis =  Math.cos(Math.sin(pi*lat1/180)  + Math.sin(pi*lat2/180) + Math.cos(pi*lat1/180) + Math.cos(pi*lat2/180)
                  ///      + Math.cos(pi*lon1/180 - pi*lon2/180))*6371*1000;

                //DecimalFormat decimalFormat = new DecimalFormat("0.00");


                return String.valueOf(Math.round(d*1000));

            }*/
        }
        catch (Exception ex)
        {
            // return  "0.0";
        }

        return "0.0";
    }


    public static double deg2rad(double deg) {
        return deg * (Math.PI/180);
    }

    public static Boolean isListEmpty(List<Object> collections) {
        try {
            if (collections.size() > 0) {
                return false;
            }
        } catch (Exception ex) {

        }
        /*if ((collections == null) || collections.isEmpty())
        {
            return true;
        }*/

        return true;
    }


    public static boolean isEmptyArray(Object[] array) {
        try {

            if (array != null && array.length > 0) {
                return false;
            }

        } catch (Exception ex) {

        }
        return true;
    }


    public static int getBatteryPercentage(Context context) {


        IntentFilter iFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = context.registerReceiver(null, iFilter);

        int level = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1) : -1;
        int scale = batteryStatus != null ? batteryStatus.getIntExtra(BatteryManager.EXTRA_SCALE, -1) : -1;

        float batteryPct = level / (float) scale;

        return (int) (batteryPct * 100);


    }

    public static String getBearing(double latitude, double longitude) {
        try {
            Location targetLocation = new Location("");//provider name is unnecessary
            targetLocation.setLatitude(latitude);//your coords of course
            targetLocation.setLongitude(longitude);
            return String.valueOf(targetLocation.getBearing());
        } catch (Exception ex) {

        }

        return "";
    }

    public static String getDeviceId(Context context) {
        return Settings.Secure.getString(context.getContentResolver(),
                Settings.Secure.ANDROID_ID);
    }

    public static String getDistance(LatLng latLng, String latitude, String longitude) {

        if (latLng != null) {
            if (isDouble(latitude) && isDouble(longitude)) {
                //Location serviceLocation = new Location("");
                //serviceLocation.setLongitude(Double.parseDouble(latitude));
                //serviceLocation.setLongitude(Double.parseDouble(longitude));

                double lat = Double.parseDouble(latitude);
                double lon = Double.parseDouble(longitude);

                DecimalFormat decimalFormat = new DecimalFormat("0.00");
                float[] results = new float[1];
                Location.distanceBetween(latLng.latitude, latLng.longitude, lat, lon, results);
                //int distanceInKms = Math.round(currentLocation.distanceTo(serviceLocation) / 1000);
                return String.valueOf(decimalFormat.format(results[0] / 1000));

            }
        }
        return "0";
    }

    public static boolean isDouble(String value) {
        try {
            Double.parseDouble(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }
}
