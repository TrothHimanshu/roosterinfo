package com.trothmatrix.roosterinfo.Classes;

import org.json.JSONObject;

public class JSONHelper {

    public static Boolean ValidateStringResponse(String strResponse) {
        try {

            if (strResponse == null) {
                throw new Exception("Error: Data not received from Server.");
            }

            JSONObject response = new JSONObject(strResponse);

            if (response.isNull("status")) {
                throw new Exception("Error: Response not received from Server.");
            }

            Integer status = response.getInt("status");
            if (TextHelper.StringIsNullOrEmpty(String.valueOf(status))) {
                throw new Exception("Error: Response not received from Server.");
            }

            /*if (status.equals(0)) {
                throw new Exception("Error: Response not received from Server.");
            }*/

            return true;
        } catch (Exception ex) {
            // Ignore.
        }

        return false;
    }



    public static Boolean ValidateJSONObjectResponse(JSONObject response) {
        try {
            if (response == null) {
                throw new Exception("Error: Data not received from Server.");
            }

            if (response.isNull("status")) {
                throw new Exception("Error: Response not received from Server.");
            }

            Integer status = response.getInt("status");
            if (TextHelper.StringIsNullOrEmpty(String.valueOf(status))) {
                throw new Exception("Error: Response not received from Server.");
            }

            /*if (status.equals(0)) {
                throw new Exception("Error: Response not received from Server.");
            }*/

            return true;
        } catch (Exception ex) {
            // Ignore.
        }

        return false;
    }



    public static Boolean ValidateSuccess(JSONObject response) {
        try {

            if (response.isNull("message")) {
                throw new Exception("Error: Response not received from Server.");
            }

            String message = response.getString("message");
            if (TextHelper.StringIsNullOrEmpty(String.valueOf(message))) {
                throw new Exception("Error: Response not received from Server.");
            }

            if (message.equals("error")) {
                throw new Exception("Error: Response not received from Server.");
            }

        } catch (Exception ex) {
            // Ignore.
        }

        return false;
    }
}
