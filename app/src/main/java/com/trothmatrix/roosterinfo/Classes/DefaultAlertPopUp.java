package com.trothmatrix.roosterinfo.Classes;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Button;

import androidx.appcompat.app.AlertDialog;
import androidx.core.content.ContextCompat;

import com.trothmatrix.roosterinfo.R;


public class DefaultAlertPopUp
{

    @SuppressLint("NewApi")
    public static void TwoButtonAlertPopUp(Context context, String title, String message,
                                           String postiveButtonText, String negativeButtonText, Boolean isColoredButton,
                                           int postiveButtonColor, int negativeButtonColor , final TwoButtonPopUpInterface listner)
    {


        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(postiveButtonText , new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listner.onClickPositveButton();
            }
        });

        builder.setNegativeButton(negativeButtonText, new DialogInterface.OnClickListener() {

            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Do nothing
                dialog.dismiss();
                listner.onClickNegativeButton();
            }
        });




        AlertDialog alert = builder.create();
        alert.show();
        if(isColoredButton)
        {
            Button nbutton = alert.getButton(DialogInterface.BUTTON_NEGATIVE);
            nbutton.setTextColor(ContextCompat.getColor(context, R.color.red));

            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(ContextCompat.getColor(context, R.color.blue));

        }
    }


    @SuppressLint("NewApi")
    public static void OkButtonAlertPopUp(Context context, String title, String message, String okButtonText,
                                          Boolean isColoredButton, int okButtonColor, final OkButtonPopUpInterface listner)
    {

        try {




        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(title);
        builder.setMessage(message);

        builder.setPositiveButton(okButtonText , new DialogInterface.OnClickListener() {

            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
                listner.onClickOkButton();
            }
        });



        AlertDialog alert = builder.create();
        alert.show();
        if(isColoredButton)
        {
            Button pbutton = alert.getButton(DialogInterface.BUTTON_POSITIVE);
            pbutton.setTextColor(ContextCompat.getColor(context, R.color.blue));

        }

        }
        catch (Exception ex)
        {

            String sd="";
        }
    }
}
