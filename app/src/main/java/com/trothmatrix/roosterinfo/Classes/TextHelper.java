package com.trothmatrix.roosterinfo.Classes;

import android.widget.EditText;
import android.widget.TextView;

import com.google.android.material.textfield.TextInputLayout;

import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class TextHelper {


    public static void  setUniCodeText(TextView textView, int code)
    {
        String x = String.valueOf(Character.toChars(code));
        textView.setText(x);
    }

    public static String CapitalizeFirstLetter(String text) {
        StringBuilder rackingSystemSb = new StringBuilder(text);
        rackingSystemSb.setCharAt(0, Character.toUpperCase(rackingSystemSb.charAt(0)));
        return rackingSystemSb.toString();
    }


    public static class ValidationHelper
    {
        public static Boolean validateEmail(String text, TextInputLayout textInputLayout, String errorText)
        {
            if (TextHelper.StringIsNullOrEmpty(text.trim()) || (!isValidEmail(text)) ) {
                textInputLayout.setErrorEnabled(true);
                textInputLayout.setError(errorText);

                return false;

            } else {
                textInputLayout.setErrorEnabled(false);
                textInputLayout.setError(null);
            }

            return true;
        }

        public static Boolean validateEmail(EditText editText, String errorText)
        {
            if (TextHelper.StringIsNullOrEmpty(editText.getText().toString().trim())
                    || (!isValidEmail(editText.getText().toString())) ) {
                editText.setError(errorText);

                return false;

            } else {
                editText.setError(null);
            }

            return true;
        }



        public static boolean ValidateListOfEditTextWithInTextInputLayout (Object...  params)
        {
            for(int i=0;i<params.length;i=i+3)
            {
                boolean validation = ValidateEditTextWithInTextInputLayout((TextInputLayout)params[i],(EditText) params[i+1],
                        ((String)params[i+2]));

                if(!validation)
                {
                    return false;
                }
            }

            return true;
        }


        public static Boolean ValidateEditTextWithInTextInputLayout(TextInputLayout inputLayout, EditText editText, String errorMsg) {
            if (TextHelper.StringIsNullOrEmpty(editText.getText().toString())) {

                inputLayout.setErrorEnabled(true);
                inputLayout.setError(errorMsg);


                return false;
            }
            else
            {
                inputLayout.setErrorEnabled(false);
                inputLayout.setError(null);

            }


            return true;
        }

public static boolean ValidateListOfEditText(Object...  params)
        {
            for(int i=0;i<params.length;i=i+2)
            {
                boolean validation = ValidateEditTextNotEmpty((EditText) params[i],
                        ((String)params[i+1]));

                if(!validation)
                {
                    return false;
                }
            }

            return true;
        }
		
		

        public static Boolean ValidateEditTextNotEmpty(EditText editText, String errorText)
        {
            if (TextHelper.StringIsNullOrEmpty(editText.getText().toString().trim())) {
                editText.setError(errorText);

                return false;

            } else {
                editText.setError(null);
            }

            return true;
        }


    }

    public static Boolean StringIsNullOrEmpty(String str) {
        try {
            if (str == null || str.trim().equals("")) {
                return true;
            }
        } catch (Exception ex) {
            //Ignore.
        }

        return false;
    }

    public static boolean isValidEmail(String email) {

        String emailPattern = "[a-zA-Z0-9._-]+@[a-z]+\\.+[a-z]+";
        if (email.trim().matches(emailPattern) && email.length() > 0)
        {

            return  true;
        }
        return  false;
        //return (!TextUtils.isEmpty(target) && Patterns.EMAIL_ADDRESS.matcher(target).matches());
    }

    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[A-Za-z])(?=.*([0-9]|[$@$!%*#?&]))[A-Za-z(\\d)|($@$!%*#?&)]{8,15}$";
        //"^(?=.*?[A-Za-z])(?=.*?[0-9]).(?=.*?[#?!@$%^&*-]).{8,15}$";
        //"^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }



}
