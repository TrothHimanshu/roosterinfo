package com.trothmatrix.roosterinfo.Classes;

public interface ResponseInterface {


    void onResponse(Object... response);
    void onError(String message);

}
