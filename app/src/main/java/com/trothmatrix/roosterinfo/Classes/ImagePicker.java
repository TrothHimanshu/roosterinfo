package com.trothmatrix.roosterinfo.Classes;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.core.content.FileProvider;


import com.trothmatrix.roosterinfo.R;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import static android.Manifest.permission.CAMERA;
import static android.app.Activity.RESULT_OK;

public class ImagePicker {

    public static class ImageParameters {
        public Uri photoURI;
        public String currentPhotoPath;
        public Bitmap bitmap;
        public ByteArrayOutputStream byteArrayOutputStream;
    }


    public static final int MY_REQUEST_CODE = 100;
    public static int PICK_IMAGE_CAMERA = 1;
    public static int PICK_IMAGE_GALLERY = 2;


    public static ImageParameters selectImage(final Activity activity) {

        final ImageParameters imageParameters = new ImageParameters();

        try {
            PackageManager pm = activity.getPackageManager();
            int hasPerm = pm.checkPermission(CAMERA, activity.getPackageName());
            if (hasPerm == PackageManager.PERMISSION_GRANTED) {

                final Dialog dialog = new Dialog(activity);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(R.layout.popup_gallary_camera);
                dialog.setCancelable(true);
                //dialog.getWindow().setBackgroundDrawableResource(R.color.transparent);

                //window.setGravity(Gravity.CENTER);

                View v = dialog.getWindow().getDecorView();
                v.setBackgroundResource(android.R.color.transparent);
                //window.setLayout(ActionBar.LayoutParams.MATCH_PARENT, ActionBar.LayoutParams.MATCH_PARENT);
                dialog.show();
                //window.setGravity(Gravity.CENTER);


                dialog.findViewById(R.id.cancel).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                dialog.findViewById(R.id.gallary).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();

                        Intent pickPhoto = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        activity.startActivityForResult(pickPhoto, PICK_IMAGE_GALLERY);



                    }
                });

                dialog.findViewById(R.id.camera).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();


                        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File photoFile = null;
                        try {
                            photoFile = createTimeBasedImageFile(activity);
                            imageParameters.currentPhotoPath = photoFile.getAbsolutePath();
                        } catch (IOException ex) {
                            // Error occurred while creating the File

                        }

                        // Continue only if the File was successfully created
                        if (photoFile != null) {

                            imageParameters.photoURI = FileProvider.getUriForFile(activity,
                                    activity.getPackageName() + ".fileprovider",
                                    photoFile);
                            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, imageParameters.photoURI);

                            activity.startActivityForResult(takePictureIntent, PICK_IMAGE_CAMERA);

                        }




                    }
                });










            } else
                Toast.makeText(activity, "Camera Permission error", Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Toast.makeText(activity, "Camera Permission error", Toast.LENGTH_SHORT).show();
            //e.printStackTrace();
        }
        return imageParameters;
    }


    public static ImageParameters onRequestPermissionsResult(int requestCode,
                                                             String permissions[], int[] grantResults,
                                                             Activity activity) {

        ImageParameters imageParameters = new ImageParameters();
        switch (requestCode) {

            case MY_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    imageParameters = selectImage(activity);

                } else {
                    Toast.makeText(activity, "Permission denied", Toast.LENGTH_SHORT).show();

                }
        }
        return imageParameters;
    }


    public static File createTimeBasedImageFile(Activity activity) throws IOException {
        // Create an image file name
        try {
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            String imageFileName = "JPEG_" + timeStamp + "_";
            File storageDir = activity.getExternalFilesDir(Environment.DIRECTORY_PICTURES);
            File image = File.createTempFile(
                    imageFileName,  /* prefix */
                    ".jpg",         /* suffix */
                    storageDir      /* directory */
            );

            // Save a file: path for use with ACTION_VIEW intents
            //mCurrentPhotoPath = image.getAbsolutePath();
            return image;
        } catch (Exception ex) {
            //ShowMessage(ex.getMessage());
        }
        return null;
    }


    @SuppressLint("NewApi")
    public static ImageParameters onActivityResult(int requestCode, int resultCode, Intent data, Activity activity,
                                                   ImageParameters imageParameters, ImageView imageView) {
        try {
            if (resultCode == RESULT_OK) {

                if (requestCode == PICK_IMAGE_CAMERA) {
                    try {


                        // optional add pic to gallery
                        // SendSignalToGalleryOfMediaUpdation();


                        imageParameters.bitmap = handleSamplingAndRotationBitmap(activity, imageParameters.photoURI);
                        //aspectRatio = ImageUtilities.GetAspectRatio(bitmap);
                        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                        imageParameters.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);

                        imageParameters.byteArrayOutputStream = bytes;

                        imageView.setImageResource(0);
                        imageView.setImageDrawable(null);
                        imageView.setImageBitmap(null);
                        imageView.setBackground(null);
                        imageView.setBackgroundResource(0);
                        imageView.setImageURI(null);
                        imageView.invalidate();

                        GlideHelper.LoadLocalCircularImage(activity,imageParameters.byteArrayOutputStream.toByteArray(),imageView);
                        //imageView.setImageBitmap(imageParameters.bitmap);


                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else if (requestCode == PICK_IMAGE_GALLERY) {

                    imageParameters.photoURI = data.getData();

                    try {
                        imageParameters.bitmap = handleSamplingAndRotationBitmap(activity, imageParameters.photoURI);
                        //aspectRatio = ImageUtilities.GetAspectRatio(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                    ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                    imageParameters.bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                    imageParameters.byteArrayOutputStream = bytes;

                    imageView.setImageResource(0);
                    imageView.setImageDrawable(null);
                    imageView.setImageBitmap(null);
                    imageView.setBackground(null);
                    imageView.setBackgroundResource(0);
                    imageView.setImageURI(null);
                    imageView.invalidate();



                    GlideHelper.LoadLocalCircularImage(activity,imageParameters.byteArrayOutputStream.toByteArray(),imageView);
                    //imageView.setImageBitmap(imageParameters.bitmap);


                }
            }
        } catch (Exception ex) {
            //ShowMessage(ex.getMessage());
        }
        return imageParameters;

    }


    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }


    private static int calculateInSampleSize(BitmapFactory.Options options,
                                             int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    private static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    private static Bitmap rotateImage(Bitmap img, int degree) {
        Matrix matrix = new Matrix();
        matrix.postRotate(degree);
        Bitmap rotatedImg = Bitmap.createBitmap(img, 0, 0, img.getWidth(), img.getHeight(), matrix, true);
        img.recycle();
        return rotatedImg;
    }



}
